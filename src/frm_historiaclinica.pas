unit frm_historiaclinica;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, dbdateedit, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, Buttons, DBGrids, StdCtrls, DbCtrls, Grids
  , dmgeneral
  ;

type

  { TfrmHistoriaClinica }

  TfrmHistoriaClinica = class(TForm)
    BitBtn1: TBitBtn;
    btnEliminar: TBitBtn;
    btnClose: TBitBtn;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    ds_HC: TDatasource;
    DBDateEdit1: TDBDateEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBGrid1: TDBGrid;
    dsPacientes: TDatasource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure BitBtn1Click(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure DBGrid1PrepareCanvas(sender: TObject; DataCol: Integer;
      Column: TColumn; AState: TGridDrawState);
    procedure Label9Click(Sender: TObject);
  private
    function SeguridadValida(nivel: integer): boolean;
  public
  end;

var
  frmHistoriaClinica: TfrmHistoriaClinica;

implementation
{$R *.lfm}
uses
    frm_login
  , frm_tratamientoae
  , dm_tratamientos
  , dm_docentes

  ;

{ TfrmHistoriaClinica }

procedure TfrmHistoriaClinica.btnCloseClick(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

procedure TfrmHistoriaClinica.btnEliminarClick(Sender: TObject);
begin
   if SeguridadValida(NIVEL_DOC_TITULAR) then
   begin
     if (MessageDlg ('ATENCION', 'Elimino el tratamiento seleccionado?', mtConfirmation, [mbYes, mbNo],0 ) = mrYes) then
     begin
       DMTratamientos.Del(ds_HC.DataSet.FieldByName('tratamiento_id').AsString);
       DMTratamientos.HistoriaClinica(DMTratamientos.HCpaciente_id.asString);
     end;
   end;
end;

procedure TfrmHistoriaClinica.DBGrid1PrepareCanvas(sender: TObject;
  DataCol: Integer; Column: TColumn; AState: TGridDrawState);
begin
  if Column.FieldName='fEvaluacion' then
  begin
    if Column.Field.AsDateTime  = 0 then
    begin
      with (Sender As TDBGrid) do
      begin
        Canvas.Font.Color:=Canvas.Brush.Color;
        Canvas.Font.Style:=[fsBold];
      end;
    end;
  end;

  if Column.FieldName='Evaluacion' then
  begin
    if Column.Field.AsFloat < 4 then
    begin
      with (Sender As TDBGrid) do
      begin
        Canvas.Font.Color:=clRed;
        Canvas.Font.Style:=[fsBold];
      end;
      if Column.Field.AsFloat  = -1 then
      begin
        with (Sender As TDBGrid) do
        begin
          Canvas.Font.Color:=Canvas.Brush.Color;
          Canvas.Font.Style:=[fsBold];
        end;
      end;
    end;
  end;
end;

procedure TfrmHistoriaClinica.BitBtn1Click(Sender: TObject);
var
  frm: TfrmTratamientoAE;
begin
   frm := TfrmTratamientoAE.Create(self);
   try
     if SeguridadValida(NIVEL_DOC_TITULAR) then
     begin
       frm.treatment_id := DMTratamientos.HCtratamiento_id.asString;
       frm.student_id := DMTratamientos.HCalumno_id.asString;
       frm.ShowModal;
       DMTratamientos.HistoriaClinica(DMTratamientos.HCpaciente_id.asString);
     end;
   finally
     frm.Free;
   end;
end;

procedure TfrmHistoriaClinica.Label9Click(Sender: TObject);
begin

end;

function TfrmHistoriaClinica.SeguridadValida(nivel: integer): boolean;
var
  pass: TfrmLogin;
begin
  pass := TfrmLogin.Create(self);
  try
    pass.NivelMinimo := nivel;
    Result := (pass.ShowModal = mrOk);
  finally
    pass.Free;
  end;
end;

end.

