unit dm_alumnos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, rxmemds, ZDataset
  , dm_conexion
  , dmgeneral, db
  ;

type

  { TDMAlumnos }

  TDMAlumnos = class(TDataModule)
    Alumnosapellidos: TStringField;
    Alumnosbarra: TLongintField;
    AlumnosBusDocAPELLIDOS: TStringField;
    AlumnosBusDocBARRA: TLongintField;
    AlumnosBusDocCLINICA_ID: TLongintField;
    AlumnosBusDocCURSO_ID: TLongintField;
    AlumnosBusDocDOCUMENTO: TLongintField;
    AlumnosBusDocID: TStringField;
    AlumnosBusDocLEGAJO: TLongintField;
    AlumnosBusDocNOMBRES: TStringField;
    AlumnosBusLegajo: TZQuery;
    AlumnosBusLegajoAPELLIDOS: TStringField;
    AlumnosBusLegajoBARRA: TLongintField;
    AlumnosBusLegajoCLINICA_ID: TLongintField;
    AlumnosBusLegajoCURSO_ID: TLongintField;
    AlumnosBusLegajoDOCUMENTO: TLongintField;
    AlumnosBusLegajoID: TStringField;
    AlumnosBusLegajoLEGAJO: TLongintField;
    AlumnosBusLegajoNOMBRES: TStringField;
    Alumnosclinica_id: TLongintField;
    Alumnoscurso_id: TLongintField;
    Alumnosdocumento: TLongintField;
    Alumnosid: TStringField;
    Alumnoslegajo: TLongintField;
    Alumnosnombres: TStringField;
    AlumnosBusDoc: TZQuery;
    AlumnosDEL: TZQuery;
    qStdAvg: TZQuery;
    qClinicsByID: TZQuery;
    qCourses: TZQuery;
    qClinics: TZQuery;
    Alumnos: TRxMemoryData;
    AlumnosINS: TZQuery;
    AlumnosUPD: TZQuery;
    AlumnosSEL: TZQuery;
    qCourseByID: TZQuery;
    qStdAvgCANTIDAD: TLongintField;
    qStdAvgTOTAL: TFloatField;
    procedure AlumnosAfterInsert(DataSet: TDataSet);
  private
    StudentId: GUID_ID;
  public
    procedure Save;
    function New: GUID_ID;
    procedure Edit (student_id: GUID_ID);
    procedure Del (student_id: GUID_ID);

    procedure EditCB (Course, clinic: integer);

    function StudentByLegajo (legajo: integer): GUID_ID;
    function StudentByDocumento (documento: integer): GUID_ID;
    procedure StudentByID (stdID: GUID_ID);

    function CourseName (CourseID: integer): string;
    function ClinicName (ClinicID: integer): string;

    function StudentAvg (stdID: GUID_ID; year: integer): double;
  end;


var
  DMAlumnos: TDMAlumnos;

implementation

{$R *.lfm}

{ TDMAlumnos }

procedure TDMAlumnos.AlumnosAfterInsert(DataSet: TDataSet);
begin
  StudentId:= DM_General.CrearGUID;
  Alumnosid.AsString:= StudentId;
  Alumnosapellidos.AsString:= EmptyStr;
  Alumnosnombres.AsString:= EmptyStr;
 // Alumnoslegajo.asInteger:= 0;
  Alumnosbarra.asInteger:= 0;
//  Alumnosdocumento.asInteger:= 0;
  Alumnoscurso_id.asInteger:= 0;
  Alumnosclinica_id.asInteger:= 0;
end;

procedure TDMAlumnos.Save;
begin
  DM_General.GrabarDatos(AlumnosSEL, AlumnosINS, AlumnosUPD, Alumnos, 'id');
end;

function TDMAlumnos.New: GUID_ID;
begin
  DM_General.ReiniciarTabla(Alumnos);
  Alumnos.Insert;
  Result:= StudentId;
end;

procedure TDMAlumnos.Edit(student_id: GUID_ID);
begin
  studentID:=student_id;
  DM_General.ReiniciarTabla(Alumnos);
  with AlumnosSEL do
  begin
    if active then close;
    ParamByName('id').asString:= student_id;
    Open;
    Alumnos.LoadFromDataSet(AlumnosSEL, 0, lmAppend);
    close;
    Alumnos.Edit;
  end;
end;

procedure TDMAlumnos.Del(student_id: GUID_ID);
begin
  with AlumnosDEL do
  begin
    ParamByName('id').asString:= student_id;
    ExecSQL;
  end;
end;

procedure TDMAlumnos.EditCB(Course, clinic: integer);
begin
  With Alumnos do
  begin
    Edit;
    Alumnoscurso_id.asInteger:= course;
    Alumnosclinica_id.asInteger:= clinic;
    Post;
  end;
end;

function TDMAlumnos.StudentByLegajo(legajo: integer): GUID_ID;
begin
  with AlumnosBusLegajo do
  begin
    if active then close;
    ParamByName('legajo').asInteger:= legajo;
    Open;
    if RecordCount > 0 then
      Result:= AlumnosBusLegajoID.AsString
    else
      Result:= GUIDNULO;
  end;
end;

function TDMAlumnos.StudentByDocumento(documento: integer): GUID_ID;
begin
  with AlumnosBusDoc do
  begin
    if active then close;
    ParamByName('documento').asInteger:= documento;
    Open;
    if RecordCount > 0 then
      Result:= AlumnosBusDocID.AsString
    else
      Result:= GUIDNULO;
  end;

end;

procedure TDMAlumnos.StudentByID(stdID: GUID_ID);
begin
  DM_General.ReiniciarTabla(Alumnos);
  With AlumnosSEL do
  begin
    if active then close;
    ParamByName('id').AsString:= stdID;
    Alumnos.LoadFromDataSet(AlumnosSEL, 0, lmAppend);
    close;
  end;
end;

function TDMAlumnos.CourseName(CourseID: integer): string;
begin
  with qCourseByID do
  begin
    if active then close;
    ParamByName('id').AsInteger:= CourseID;
    Open;
    IF RecordCount > 0 then
      Result:= qCourseByID.FieldByName('curso').asString
    else
      Result:= 'ERROR al obtener el nombre del curso';
  end;
end;

function TDMAlumnos.ClinicName(ClinicID: integer): string;
begin
  with qClinicsByID do
  begin
    if active then close;
    ParamByName('id').AsInteger:= ClinicID;
    Open;
    IF RecordCount > 0 then
      Result:= qClinicsByID.FieldByName('clinica').asString
    else
      Result:= 'ERROR al obtener el nombre de la clinica';
  end;
end;

function TDMAlumnos.StudentAvg(stdID: GUID_ID; year: integer): double;
begin
  with qStdAvg do
  begin
    if active then close;
    ParamByName ('alumno_id').asString:= stdID;
    ParamByName ('ano').asInteger:= year;
    Open;
    if ((RecordCount > 0) and (qStdAvgCANTIDAD.AsFloat <> 0)) then
      Result:= FieldByName('total').asFloat / FieldByName('cantidad').asFloat
    else
      Result:= 0;
  end;
end;

end.

