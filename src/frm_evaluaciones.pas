unit frm_evaluaciones;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, DBGrids, EditBtn, StdCtrls, DbCtrls
  , dmgeneral, db, Grids, types;

type

  { TfrmEvaluaciones }

  TfrmEvaluaciones = class(TForm)
    btnFilter: TBitBtn;
    btnSave: TBitBtn;
    cbClinica: TComboBox;
    cbCurso: TComboBox;
    dbEvaluacion: TDBComboBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DS_evaluaciones: TDatasource;
    edFechaIni: TDateEdit;
    DBGrid1: TDBGrid;
    edFechaFin: TDateEdit;
    edLegajo: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edPromedio: TLabeledEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    rgCriterio: TRadioGroup;
    procedure btnFilterClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure dbEvaluacionChange(Sender: TObject);
    procedure dbEvaluacionCloseUp(Sender: TObject);
    procedure DBGrid1PrepareCanvas(sender: TObject; DataCol: Integer;
      Column: TColumn; AState: TGridDrawState);
    procedure DS_evaluacionesDataChange(Sender: TObject; Field: TField);
    procedure DS_evaluacionesUpdateData(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
  private
    teacherID: GUID_ID;
    teacherLevel: integer;
    procedure Initialize;
  public
    property teacher_id: GUID_ID read teacherID write teacherID;
    property teacher_level: integer read teacherLevel write teacherLevel;
  end;

var
  frmEvaluaciones: TfrmEvaluaciones;

implementation

{$R *.lfm}
uses
  dm_alumnos
  , dm_docentes
  , dm_tratamientos
  , dateutils
  ;


{ TfrmEvaluaciones }

procedure TfrmEvaluaciones.FormShow(Sender: TObject);
begin
  Initialize;
end;

procedure TfrmEvaluaciones.Panel2Click(Sender: TObject);
begin

end;

procedure TfrmEvaluaciones.Panel3Click(Sender: TObject);
begin

end;

procedure TfrmEvaluaciones.Initialize;
begin
  edFechaIni.Date := EncodeDate(YearOf(Now), 1,1);
  edFechaFin.Date := NOW;
  DM_General.CargarComboBox(cbCurso, 'curso', 'id', DMAlumnos.qCourses);
  DM_General.CargarComboBox(cbClinica, 'clinica', 'id', DMAlumnos.qClinics);
end;

procedure TfrmEvaluaciones.btnFilterClick(Sender: TObject);
var
  idClinica, idCurso: integer;
begin
  idClinica:= DM_General.obtenerIDIntComboBox(cbClinica);
  idCurso:= DM_General.obtenerIDIntComboBox(cbCurso);
  DMDocentes.teacherByID(teacherID);
  case rgCriterio.ItemIndex of
    0: begin
         DMTratamientos.FiltrarEvalLegajo(edFechaIni.Date, edFechaFin.Date, StrtoIntDef(edLegajo.Text, 0));
       end;
    1: begin
         if (DMDocentes.CursoPropio (idCurso) )
           OR (DMDocentes.Docentesnivel.asInteger < NIVEL_DOC_CLASE) then
         begin
           DMTratamientos.FiltrarEvaluaciones(edFechaIni.Date, edFechaFin.Date, idClinica, idCurso);
         end
         else
           ShowMessage ('No tiene permisos para evaluar una clase no propia');
      end;
  end;

end;

procedure TfrmEvaluaciones.btnSaveClick(Sender: TObject);
begin
  with DMTratamientos, Tratamientos do
  begin
    DisableControls;
    First;
    while not eof do
    begin
      if ((Tratamientosevaluacion.AsFloat > -1)
          and (Tratamientosdocente_id.AsString = GUIDNULO)
          )then
      begin
         Edit;
         Tratamientosdocente_id.AsString:= teacherID;
         TratamientosfEvaluacion.AsDateTime:= Dateof (now);
         Post;
      end;

      Next;
    end;
    EnableControls;
  end;
  DMTratamientos.Save;
  ModalResult:= mrOK;
end;

procedure TfrmEvaluaciones.dbEvaluacionChange(Sender: TObject);
begin

end;


procedure TfrmEvaluaciones.dbEvaluacionCloseUp(Sender: TObject);
begin
  if ((DMTratamientos.Tratamientosevaluacion.AsFloat > -1)
     and (teacherLevel > NIVEL_DOC_TITULAR) ) then
  begin
    DMTratamientos.Tratamientos.Cancel;
    showMessage ('Solo el Titular puede modificar notas');
    DMTratamientos.Tratamientos.Edit;
  end
  //else
  //begin
  //  if DMTratamientos.Tratamientos.State in dsEditModes then
  //    DMTratamientos.Tratamientos.Post;
  //end;
end;


procedure TfrmEvaluaciones.DBGrid1PrepareCanvas(sender: TObject;
  DataCol: Integer; Column: TColumn; AState: TGridDrawState);
begin
   if Column.FieldName='evaluacion' then
    begin
      if Column.Field.AsFloat < 4 then
      begin
        with (Sender As TDBGrid) do
        begin
          //Custom drawing
//          Canvas.Brush.Color:=clYellow;
          Canvas.Font.Color:=clRed;
          Canvas.Font.Style:=[fsBold];
        end;
        if Column.Field.AsFloat  = -1 then
        begin
          with (Sender As TDBGrid) do
          begin
            //Custom drawing
  //          Canvas.Brush.Color:=clYellow;
            Canvas.Font.Color:=Canvas.Brush.Color;
            Canvas.Font.Style:=[fsBold];
          end;
        end;
      end;
    end;
end;

procedure TfrmEvaluaciones.DS_evaluacionesDataChange(Sender: TObject;
  Field: TField);
begin

    if DMTratamientos.Tratamientosevaluacion.AsFloat = -1 then
  begin
    with (dbEvaluacion) do
    begin
      Text:= '0';
    end;
  end;





   if ((DMTratamientos.Tratamientosevaluacion.AsFloat > -1)
      and (teacherLevel > NIVEL_DOC_TITULAR) ) then
     dbEvaluacion.ReadOnly:= true
   else
     dbEvaluacion.ReadOnly:= false;
   edPromedio.Text:= FormatFloat('####0.00', DMAlumnos.StudentAvg(DMTratamientos.Tratamientosalumno_id.asString, YearOf(edFechaFin.Date)));
end;



procedure TfrmEvaluaciones.DS_evaluacionesUpdateData(Sender: TObject);
begin
end;


end.


