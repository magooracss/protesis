unit frm_main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ActnList,
  Menus, ComCtrls, ExtCtrls, StdCtrls
  , dmgeneral;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    teacherStadistics: TAction;
    edNroAlumno: TEdit;
    Imgfondo: TImage;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem24: TMenuItem;
    MenuItem25: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    Panel1: TPanel;
    prgExit: TAction;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    stTitular: TStaticText;
    teachTreatment: TAction;
    teachEval: TAction;
    teachDel: TAction;
    teachEdit: TAction;
    teachNew: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    treatHC: TAction;
    treatNew: TAction;
    studQualification: TAction;
    prgAbout: TAction;
    patDel: TAction;
    prgConfig: TAction;
    prgEditReport: TAction;
    prgEditTUG: TAction;
    studNew: TAction;
    studEdit: TAction;
    studDel: TAction;
    patNew: TAction;
    patEdit: TAction;
    ActionList1: TActionList;
    procedure edNroAlumnoExit(Sender: TObject);
    procedure edNroAlumnoKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure patDelExecute(Sender: TObject);
    procedure patEditExecute(Sender: TObject);
    procedure patNewExecute(Sender: TObject);
    procedure prgAboutExecute(Sender: TObject);
    procedure prgConfigExecute(Sender: TObject);
    procedure prgEditTUGExecute(Sender: TObject);
    procedure prgExitExecute(Sender: TObject);
    procedure studDelExecute(Sender: TObject);
    procedure studEditExecute(Sender: TObject);
    procedure studNewExecute(Sender: TObject);
    procedure teachDelExecute(Sender: TObject);
    procedure teachEditExecute(Sender: TObject);
    procedure teachEvalExecute(Sender: TObject);
    procedure teachNewExecute(Sender: TObject);
    procedure treatHCExecute(Sender: TObject);
    procedure treatNewExecute(Sender: TObject);
  private
    procedure Inicializar;
    procedure StudentForm(refStudent: GUID_ID);
    procedure PatientForm(refPatient: GUID_ID);
    procedure TreatmentForm(refTreatment, refStudent: GUID_ID);
    procedure TeacherForm(refTeacher: GUID_ID);
    function SeguridadValida(nivel: integer): boolean;
  public
    { public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}
uses
  frm_about
  , SD_Configuracion
  , frm_alumnoae
  , dm_alumnos
  , frm_pacienteae
  , dm_pacientes
  , frm_tratamientoae
  , frm_docenteae
  , dm_docentes
  , frm_evaluaciones
  , frm_login
  , dm_tratamientos
  , frm_historiaclinica
  , frm_tugTratamientos
  , frm_historiaclinicabusqueda
  , frm_configuracion
  ;

{ TfrmMain }

procedure TfrmMain.prgExitExecute(Sender: TObject);
begin
  Application.Terminate;
end;


procedure TfrmMain.Inicializar;
begin
  stTitular.Caption := LeerDato(SECCION_TEXTOS, TEXTO_TITULAR);
  Imgfondo.Picture.LoadFromFile(LeerDato(SECCION_APP, RUTA_FONDO));
end;



procedure TfrmMain.prgAboutExecute(Sender: TObject);
var
  pant: TfrmAbout;
begin
  pant := TfrmAbout.Create(self);
  try
    pant.ShowModal;
  finally
    pant.Free;
  end;
end;

procedure TfrmMain.prgConfigExecute(Sender: TObject);
var
  pant: TfrmConfiguracion;
begin
  pant:= TfrmConfiguracion.Create(self);
  try
    if SeguridadValida(NIVEL_DOC_TITULAR) then
      pant.ShowModal;
  finally
    pant.Free;
  end;
end;

procedure TfrmMain.prgEditTUGExecute(Sender: TObject);
var
  frm: TfrmTugTratamientos;
begin
  frm:= TfrmTugTratamientos.Create(self);
  try
    if SeguridadValida(NIVEL_DOC_TITULAR) then
       frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  Inicializar;
end;

procedure TfrmMain.edNroAlumnoKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
    treatNewExecute(Sender);
end;

procedure TfrmMain.edNroAlumnoExit(Sender: TObject);
begin
  if (TRIM(edNroAlumno.Text) <> EmptyStr) then
    treatNewExecute(Sender);
end;


procedure TfrmMain.StudentForm(refStudent: GUID_ID);
var
  frm: TfrmAlumnosAE;
begin
  frm := TfrmAlumnosAE.Create(self);
  try
    frm.student_id := refStudent;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TfrmMain.studNewExecute(Sender: TObject);
begin
  StudentForm(GUIDNULO)
end;


procedure TfrmMain.studEditExecute(Sender: TObject);
var
  nroLegajo: integer;
  StdID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_CLASE) then
    nroLegajo := StrToIntDef(InputBox('Búsqueda de Alumno',
      'Ingrese Legajo sin barra', ''), -1)
  else
    nroLegajo := -1;

  if nroLegajo <> -1 then
  begin
    StdID := DMAlumnos.StudentByLegajo(nroLegajo);
    if StdID <> GUIDNULO then
      StudentForm(StdID)
    else
      ShowMessage('No se encuentra un alumno con el legajo: ' + IntToStr(nroLegajo));
  end;
end;


procedure TfrmMain.studDelExecute(Sender: TObject);
var
  nroLegajo: integer;
  StdID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_TITULAR) then
    nroLegajo := StrToIntDef(InputBox('Búsqueda de Alumno',
      'Ingrese Legajo sin barra', ''), -1)
  else
    nroLegajo := -1;

  if nroLegajo <> -1 then
  begin
    StdID := DMAlumnos.StudentByLegajo(nroLegajo);
    if StdID <> GUIDNULO then
    begin
      DMAlumnos.Del(stdID);
      ShowMessage('Alumno eliminado de la BD');
    end
    else
      ShowMessage('No se encuentra un alumno con el legajo: ' + IntToStr(nroLegajo));
  end;
end;

procedure TfrmMain.PatientForm(refPatient: GUID_ID);
var
  frm: TfrmPacienteAE;
begin
  frm := TfrmPacienteAE.Create(self);
  try
    frm.patient_id := refPatient;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TfrmMain.patNewExecute(Sender: TObject);
var
  ok: Boolean;
begin
  ok:= (LeerDato(SECCION_APP, H_P) = LEVANTAR_DATO);
  if (OK) then
    PatientForm(GUIDNULO)
  else
    if SeguridadValida(NIVEL_DOC_TITULAR) then
      PatientForm(GUIDNULO)
    else
      ShowMessage('En este período, solamente el titular puede dar de alta pacientes');
end;

procedure TfrmMain.patEditExecute(Sender: TObject);
var
  documento: integer;
  PatID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_CLASE) then
    documento := StrToIntDef(InputBox('Búsqueda de Paciente',
      'Ingrese el número de documento', ''), -1)
  else
    documento := -1;

  if documento <> -1 then
  begin
    PatID := DMPacientes.PatientByDoc(documento);
    if PatID <> GUIDNULO then
      PatientForm(PatID)
    else
      ShowMessage('No se encuentra un paciente con el documento: ' +
        IntToStr(documento));
  end;
end;

procedure TfrmMain.patDelExecute(Sender: TObject);
var
  documento: integer;
  PatID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_TITULAR) then
    documento := StrToIntDef(InputBox('Búsqueda de Paciente',
      'Ingrese el número de documento', ''), -1)
  else
    documento := -1;

  if documento <> -1 then
  begin
    PatID := DMPacientes.PatientByDoc(documento);
    if PatID <> GUIDNULO then
    begin
      DMPacientes.Del(PatID);
      ShowMessage('Paciente eliminado de la BD');
    end
    else
      ShowMessage('No se encuentra un paciente con el documento: ' +
        IntToStr(documento));
  end;
end;

procedure TfrmMain.TreatmentForm(refTreatment, refStudent: GUID_ID);
var
  frm: TfrmTratamientoAE;
begin
  frm := TfrmTratamientoAE.Create(self);
  try
    frm.treatment_id := refTreatment;
    frm.student_id := refStudent;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TfrmMain.treatNewExecute(Sender: TObject);
var
  nroLegajo: integer;
  StdID: GUID_ID;
begin
  if (Sender is TEdit) then //Simple y rapido para agregar el TEdit
    nroLegajo := StrToIntDef(edNroAlumno.Text, -1)
  else
    nroLegajo := StrToIntDef(InputBox('Búsqueda de Alumno',
      'Ingrese Legajo sin barra', ''), -1);

  if nroLegajo <> -1 then
  begin
    StdID := DMAlumnos.StudentByLegajo(nroLegajo);
    if StdID <> GUIDNULO then
    begin
      TreatmentForm(GUIDNULO, StdID);
    end
    else
      ShowMessage('No se encuentra un alumno con el legajo: ' + IntToStr(nroLegajo));
  end;
  edNroAlumno.Clear;
end;

procedure TfrmMain.TeacherForm(refTeacher: GUID_ID);
var
  frm: TfrmDocenteAE;
begin
  frm := TfrmDocenteAE.Create(self);
  try
    frm.teacher_id := refTeacher;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

function TfrmMain.SeguridadValida(nivel: integer): boolean;
var
  pass: TfrmLogin;
begin
  pass := TfrmLogin.Create(self);
  try
    pass.NivelMinimo := nivel;
    Result := (pass.ShowModal = mrOk);
  finally
    pass.Free;
  end;
end;


procedure TfrmMain.teachNewExecute(Sender: TObject);
begin
  if SeguridadValida(NIVEL_DOC_TITULAR) then
    TeacherForm(GUIDNULO);
end;

//procedure TfrmMain.teachTreatmentExecute(Sender: TObject);
//var
//  prestacion: integer;
//  tratID: GUID_ID;
//begin
//  if SeguridadValida(NIVEL_DOC_TITULAR) then
//    prestacion := StrToIntDef(InputBox('Búsqueda de Tratamiento',
//      'Ingrese el número de Prestacion', ''), -1)
//  else
//    prestacion := -1;
//
//
//  if prestacion <> -1 then
//  begin
//      tratID := DMTratamientos.treatmentByPrest (prestacion);
//      if (tratID <> GUIDNULO) then
//      begin
//        TreatmentForm(tratID, DMTratamientos.Tratamientosalumno_id.AsString);
//      end
//      else
//        ShowMessage('No se encuentra una prestaciòn con el número: ' + IntToStr(prestacion));
//  end;
//end;

procedure TfrmMain.teachEditExecute(Sender: TObject);
var
  documento: integer;
  teachID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_TITULAR) then
    documento := StrToIntDef(InputBox('Búsqueda de Docente',
      'Ingrese el número de documento', ''), -1)
  else
    documento := -1;

  if documento <> -1 then
  begin
    teachID := DMDocentes.teacherByDoc(documento);
    if teachID <> GUIDNULO then
      TeacherForm(teachID)
    else
      ShowMessage('No se encuentra un docente con el documento: ' + IntToStr(documento));
  end;
end;


procedure TfrmMain.teachDelExecute(Sender: TObject);
var
  documento: integer;
  techID: GUID_ID;
begin
  if SeguridadValida(NIVEL_DOC_TITULAR) then
    documento := StrToIntDef(InputBox('Búsqueda de Docente',
      'Ingrese el número de documento', ''), -1)
  else
    documento := -1;

  if documento <> -1 then
  begin
    techID := DMDocentes.teacherByDoc(documento);
    if techID <> GUIDNULO then
    begin
      DMDocentes.Del(techID);
      ShowMessage('Docente eliminado de la BD');
    end
    else
      ShowMessage('No se encuentra un docente con el documento: ' +
        IntToStr(documento));
  end;
end;


procedure TfrmMain.teachEvalExecute(Sender: TObject);
var
  frm: TfrmEvaluaciones;
  pass: TfrmLogin;
begin
  frm := TfrmEvaluaciones.Create(self);
  pass := TfrmLogin.Create(self);
  pass.NivelMinimo := NIVEL_DOC_CLASE;
  try
    if pass.ShowModal = mrOk then
    begin
      frm.teacher_id := pass.TeacherID;
      frm.teacher_level:= pass.NivelUsuario;
      frm.ShowModal;
    end;
  finally
    pass.Free;
    frm.Free;
  end;
end;

procedure TfrmMain.treatHCExecute(Sender: TObject);
var
  documento: integer;
  PatID: GUID_ID;
  frm: TfrmHistoriaClinica;
  frm_buscar: TfrmHistoriaClinicaBusqueda;
begin
  frm := TfrmHistoriaClinica.Create(self);
  frm_buscar:= TfrmHistoriaClinicaBusqueda.Create(self);
  try
    if frm_buscar.ShowModal = mrOK then
      PatID:= frm_buscar.paciente_id
    else
      PatID:= GUIDNULO;

    begin
//      PatID := DMPacientes.PatientByDoc(documento);
      if PatID <> GUIDNULO then
      begin
        DMTratamientos.HistoriaClinica(patID);
        DMPacientes.Edit(patID);
        frm.ShowModal;
      end ;
//      else
//        ShowMessage('No se encuentra una historia clinica con el criterio seleccionado');
    end;
  finally
    frm.Free;
  end;

end;


end.
