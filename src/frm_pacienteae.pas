unit frm_pacienteae;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, dbdateedit, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, Buttons, DbCtrls, StdCtrls
  , dmgeneral
  ;

type

  { TfrmPacienteAE }

  TfrmPacienteAE = class(TForm)
    btnSave: TBitBtn;
    cbProvincia: TComboBox;
    cbLocalidad: TComboBox;
    cbTipoDocumento: TComboBox;
    cbPais: TComboBox;
    DBRadioGroup1: TDBRadioGroup;
    dsPacientes: TDatasource;
    DBDateEdit1: TDBDateEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    btnLocalidades: TSpeedButton;
    stEdad: TStaticText;
    procedure btnLocalidadesClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure cbPaisChange(Sender: TObject);
    procedure cbProvinciaChange(Sender: TObject);
    procedure dsPacientesDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    patientDoc: integer;
    patientID: GUID_ID;
    procedure MostrarEdad;
    procedure Initialize;
  public
    property patient_id:GUID_ID read patientID write patientID;
    property patient_doc: integer write patientDoc;


  end;

var
  frmPacienteAE: TfrmPacienteAE;

implementation
{$R *.lfm}
uses
  dm_pacientes
  , frm_tuglocalidades
  ;

{ TfrmPacienteAE }



procedure TfrmPacienteAE.dsPacientesDataChange(Sender: TObject; Field: TField);
begin
  MostrarEdad;
end;

procedure TfrmPacienteAE.FormCreate(Sender: TObject);
begin
  patientDoc:= 0;
end;

procedure TfrmPacienteAE.FormShow(Sender: TObject);
begin
  Initialize;
end;

procedure TfrmPacienteAE.btnSaveClick(Sender: TObject);
begin
(*
  DMAlumnos.EditCb(DM_General.obtenerIDIntComboBox(cbCourse)
                  ,DM_General.obtenerIDIntComboBox(cbClinic)
                  );
*)
  DMPacientes.EditCb(DM_General.obtenerIDIntComboBox(cbLocalidad)
                    ,DM_General.obtenerIDIntComboBox(cbTipoDocumento)
                    );

  DMPacientes.Save;
  ModalResult:=mrOK;
end;

procedure TfrmPacienteAE.cbPaisChange(Sender: TObject);
begin
  with DMPacientes.qTugProvincias do
  begin
    if active then close;
    ParamByName('pais_id').asInteger:= DM_General.obtenerIDIntComboBox(cbPais);
    open;
  end;

  DM_General.CargarComboBoxAbierto(cbProvincia,  'Provincia','id' , DMPacientes.qTugProvincias);
  cbProvinciaChange(Sender);
end;

procedure TfrmPacienteAE.cbProvinciaChange(Sender: TObject);
begin
   with DMPacientes.qTugLocalidades do
   begin
     if active then close;
     ParamByName('provincia_id').asInteger:= DM_General.obtenerIDIntComboBox(cbProvincia);
     open;
   end;
   DM_General.CargarComboBoxAbierto(cbLocalidad,  'Localidad','id' , DMPacientes.qTugLocalidades);
end;

procedure TfrmPacienteAE.btnLocalidadesClick(Sender: TObject);
var
  frm: TfrmTugLocalidades;
begin
  frm:= TfrmTugLocalidades.Create(self);
  try
    frm.ShowModal;

     DM_General.CargarComboBox(cbPais,  'Pais','id' , DMPacientes.qTugPaises);
     cbPaisChange(cbPais);

  finally
    frm.Free;
  end;
end;

procedure TfrmPacienteAE.MostrarEdad;
begin
  stEdad.Caption:= 'Edad: ' + IntToStr (DMPacientes.EdadPaciente) + ' años';
end;

procedure TfrmPacienteAE.Initialize;
begin
  DM_General.CargarComboBox(cbTipoDocumento, 'documentoTipo','id', DMPacientes.qTiposDocumento);

  DM_General.CargarComboBox(cbPais,  'Pais','id' , DMPacientes.qTugPaises);
  cbPaisChange(cbPais);


  if patientID = GUIDNULO then
  begin
    patientID:= DMPacientes.New
  end
  else
  begin
    DMPacientes.Edit (patientID);
    cbTipoDocumento.ItemIndex:= DM_General.obtenerIdxCombo(cbTipoDocumento, DMPacientes.Pacientesdocumento_tipo.asInteger);

    DMPacientes.LocalidadesPacientes(DMPacientes.Pacienteslocalidad_id.asInteger);
    cbPais.ItemIndex:= DM_General.obtenerIdxCombo(cbPais, DMPacientes.qLocProvPaisPacientePAIS_ID.asInteger);
    cbProvincia.ItemIndex:= DM_General.obtenerIdxCombo(cbProvincia, DMPacientes.qLocProvPaisPacientePROVINCIA_ID.asInteger);
    cbLocalidad.ItemIndex:= DM_General.obtenerIdxCombo(cbLocalidad, DMPacientes.qLocProvPaisPacienteLOCALIDAD_ID.asInteger);

  end;

  if patientDoc > 0 then
  begin
    DMPacientes.Pacientes.Edit;
    DMPacientes.Pacientesdocumento.AsInteger:= patientDoc;
  end;

end;


end.

