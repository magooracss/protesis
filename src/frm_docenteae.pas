unit frm_docenteae;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, rxdbcomb, Forms, Controls, Graphics, Dialogs,
  DbCtrls, StdCtrls, ExtCtrls, Buttons, dmgeneral
  ;

type

  { TfrmDocenteAE }

  TfrmDocenteAE = class(TForm)
    btnSave: TBitBtn;
    ckVerClave: TCheckBox;
    ds_docentes: TDatasource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    dbClave: TDBEdit;
    DBEdit6: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    Usuario: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Panel1: TPanel;
    procedure btnSaveClick(Sender: TObject);
    procedure ckVerClaveChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    teacherID: GUID_ID;
    procedure Initialize;
  public
    property teacher_id: GUID_ID read teacherID write teacherID;
  end;

var
  frmDocenteAE: TfrmDocenteAE;

implementation
{$R *.lfm}
uses
  dm_docentes
  ;

{ TfrmDocenteAE }

procedure TfrmDocenteAE.FormShow(Sender: TObject);
begin
  Initialize;
end;

procedure TfrmDocenteAE.btnSaveClick(Sender: TObject);
begin
  DMDocentes.Save;
  ModalResult:=mrOK;
end;

procedure TfrmDocenteAE.ckVerClaveChange(Sender: TObject);
begin
  if ckVerClave.Checked then
    dbClave.PasswordChar:= Char('')
  else
    dbClave.PasswordChar:=Char('*');
  dbClave.Refresh;
  dbClave.SetFocus;
end;

procedure TfrmDocenteAE.Initialize;
begin
  if teacherID = GUIDNULO then
    teacherID:= DMDocentes.New
  else
    DMDocentes.Edit (teacherID);
end;

end.

