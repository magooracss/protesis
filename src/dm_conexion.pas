unit dm_conexion;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, ZConnection;

type

  { TDMConexion }

  TDMConexion = class(TDataModule)
    cnxGeneral: TZConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    { private declarations }
  public
    procedure AbrirConexion;
  end;

var
  DMConexion: TDMConexion;

implementation
{$R *.lfm}
uses
  SD_Configuracion
  ;

{ TDMConexion }

procedure TDMConexion.DataModuleCreate(Sender: TObject);
begin
  AbrirConexion;
end;

procedure TDMConexion.AbrirConexion;
  var
   base: string;
   host: string;
  begin
    if cnxGeneral.Connected then
     cnxGeneral.Disconnect;

    base:= LeerDato (SECCION_APP ,RUTA_BD) ;
    if ((base <>  ERROR_APERTURA_CFG)
        and (base <> ERROR_CFG)) then
    begin
      base := base;
      host:= LeerDato (SECCION_APP, BASE_HOST);
      try
        with cnxGeneral do
        begin
          Database:=  base;
          HostName:= host;
          Connect;
        end;
      except
        Exception.Create('Error abriendo la base de datos');
      end;
    end
    else
      Exception.Create('Error abriendo la base de datos desde CFG');
  end;

end.

