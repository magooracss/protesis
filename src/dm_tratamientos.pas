unit dm_tratamientos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, rxmemds, ZDataset
  ,dmgeneral
  ;

const
  MAX_DIENTES = 99;

type

  { TDMTratamientos }

  TDMTratamientos = class(TDataModule)
    dstugGrupos: TDatasource;
    DS_Grupos: TDatasource;
    DS_Subgrupos: TDatasource;
    GruposBVISIBLE: TSmallintField;
    GruposID: TLongintField;
    GruposTRATAMIENTOGRUPO: TStringField;
    HCactividad: TStringField;
    HCalumno: TStringField;
    HCalumno_id: TStringField;
    HCanoHC: TLongintField;
    HCdocente: TStringField;
    HCDocumentoPaciente: TLongintField;
    HCEvaluacion: TFloatField;
    HCfecha: TDateTimeField;
    HCfEvaluacion: TDateTimeField;
    HCLegajo: TLongintField;
    HCnroHC: TLongintField;
    HCPaciente: TStringField;
    HCpaciente_id: TStringField;
    HCpiezas: TStringField;
    HCprestacion: TLongintField;
    HCtratamiento: TStringField;
    HCtratamiento_id: TStringField;
    HCtxAlumno: TStringField;
    HCtxDocente: TStringField;
    HC: TRxMemoryData;
    qBusHC_ApAlumnoDOCUMENTOPACIENTE: TLongintField;
    qBusHC_ApAlumnoLEGAJO: TLongintField;
    qBusHC_ApAlumnoPACIENTE: TStringField;
    qBusHC_ApPaciente: TZQuery;
    qBusHC_ApPacienteLEGAJO: TLongintField;
    qBusHC_DocPaciente: TZQuery;
    qBusHC_DocPacienteLEGAJO: TLongintField;
    qBusHC_LegAlumno: TZQuery;
    qBusHC_ApAlumno: TZQuery;
    qBusHC_LegAlumnoDOCUMENTOPACIENTE: TLongintField;
    qBusHC_LegAlumnoLEGAJO: TLongintField;
    qBusHC_LegAlumnoPACIENTE: TStringField;
    qBusHC_NroHC: TZQuery;
    qBusHC_ApPacienteDOCUMENTOPACIENTE: TLongintField;
    qBusHC_ApPacientePACIENTE: TStringField;
    qBusHC_DocPacienteDOCUMENTOPACIENTE: TLongintField;
    qBusHC_DocPacientePACIENTE: TStringField;
    qBusHC_NroHCDOCUMENTOPACIENTE: TLongintField;
    qBusHC_NroHCLEGAJO: TLongintField;
    qBusHC_NroHCPACIENTE: TStringField;
    qHistoriaClinicaACTIVIDAD: TStringField;
    qHistoriaClinicaACTIVIDAD1: TStringField;
    qHistoriaClinicaACTIVIDAD2: TStringField;
    qHistoriaClinicaACTIVIDAD3: TStringField;
    qHistoriaClinicaACTIVIDAD4: TStringField;
    qHistoriaClinicaACTIVIDAD5: TStringField;
    qHistoriaClinicaALUMNO: TStringField;
    qHistoriaClinicaALUMNO1: TStringField;
    qHistoriaClinicaALUMNO2: TStringField;
    qHistoriaClinicaALUMNO3: TStringField;
    qHistoriaClinicaALUMNO4: TStringField;
    qHistoriaClinicaALUMNO5: TStringField;
    qHistoriaClinicaALUMNO_ID: TStringField;
    qHistoriaClinicaALUMNO_ID1: TStringField;
    qHistoriaClinicaALUMNO_ID2: TStringField;
    qHistoriaClinicaALUMNO_ID3: TStringField;
    qHistoriaClinicaALUMNO_ID4: TStringField;
    qHistoriaClinicaALUMNO_ID5: TStringField;
    qHistoriaClinicaANOHC: TLongintField;
    qHistoriaClinicaANOHC1: TLongintField;
    qHistoriaClinicaANOHC2: TLongintField;
    qHistoriaClinicaANOHC3: TLongintField;
    qHistoriaClinicaANOHC4: TLongintField;
    qHistoriaClinicaANOHC5: TLongintField;
    qHistoriaClinicaDOCENTE: TStringField;
    qHistoriaClinicaDOCENTE1: TStringField;
    qHistoriaClinicaDOCENTE2: TStringField;
    qHistoriaClinicaDOCENTE3: TStringField;
    qHistoriaClinicaDOCENTE4: TStringField;
    qHistoriaClinicaDOCENTE5: TStringField;
    qHistoriaClinicaEVALUACION: TFloatField;
    qHistoriaClinicaEVALUACION1: TFloatField;
    qHistoriaClinicaEVALUACION2: TFloatField;
    qHistoriaClinicaEVALUACION3: TFloatField;
    qHistoriaClinicaEVALUACION4: TFloatField;
    qHistoriaClinicaEVALUACION5: TFloatField;
    qHistoriaClinicaFECHA: TDateField;
    qHistoriaClinicaFECHA1: TDateField;
    qHistoriaClinicaFECHA2: TDateField;
    qHistoriaClinicaFECHA3: TDateField;
    qHistoriaClinicaFECHA4: TDateField;
    qHistoriaClinicaFECHA5: TDateField;
    qHistoriaClinicaFEVALUACION: TDateField;
    qHistoriaClinicaFEVALUACION1: TDateField;
    qHistoriaClinicaFEVALUACION2: TDateField;
    qHistoriaClinicaFEVALUACION3: TDateField;
    qHistoriaClinicaFEVALUACION4: TDateField;
    qHistoriaClinicaFEVALUACION5: TDateField;
    qHistoriaClinicaNROHC: TLongintField;
    qHistoriaClinicaNROHC1: TLongintField;
    qHistoriaClinicaNROHC2: TLongintField;
    qHistoriaClinicaNROHC3: TLongintField;
    qHistoriaClinicaNROHC4: TLongintField;
    qHistoriaClinicaNROHC5: TLongintField;
    qHistoriaClinicaPACIENTE_ID: TStringField;
    qHistoriaClinicaPACIENTE_ID1: TStringField;
    qHistoriaClinicaPACIENTE_ID2: TStringField;
    qHistoriaClinicaPACIENTE_ID3: TStringField;
    qHistoriaClinicaPACIENTE_ID4: TStringField;
    qHistoriaClinicaPACIENTE_ID5: TStringField;
    qHistoriaClinicaPIEZAS: TStringField;
    qHistoriaClinicaPIEZAS1: TStringField;
    qHistoriaClinicaPIEZAS2: TStringField;
    qHistoriaClinicaPIEZAS3: TStringField;
    qHistoriaClinicaPIEZAS4: TStringField;
    qHistoriaClinicaPIEZAS5: TStringField;
    qHistoriaClinicaPRESTACION: TLongintField;
    qHistoriaClinicaPRESTACION1: TLongintField;
    qHistoriaClinicaPRESTACION2: TLongintField;
    qHistoriaClinicaPRESTACION3: TLongintField;
    qHistoriaClinicaPRESTACION4: TLongintField;
    qHistoriaClinicaPRESTACION5: TLongintField;
    qHistoriaClinicaTRATAMIENTO: TStringField;
    qHistoriaClinicaTRATAMIENTO1: TStringField;
    qHistoriaClinicaTRATAMIENTO2: TStringField;
    qHistoriaClinicaTRATAMIENTO3: TStringField;
    qHistoriaClinicaTRATAMIENTO4: TStringField;
    qHistoriaClinicaTRATAMIENTO5: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID1: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID2: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID3: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID4: TStringField;
    qHistoriaClinicaTRATAMIENTO_ID5: TStringField;
    qHistoriaClinicaTXALUMNO: TBlobField;
    qHistoriaClinicaTXALUMNO1: TBlobField;
    qHistoriaClinicaTXALUMNO2: TBlobField;
    qHistoriaClinicaTXALUMNO3: TBlobField;
    qHistoriaClinicaTXALUMNO4: TBlobField;
    qHistoriaClinicaTXALUMNO5: TBlobField;
    qHistoriaClinicaTXDOCENTE: TBlobField;
    qHistoriaClinicaTXDOCENTE1: TBlobField;
    qHistoriaClinicaTXDOCENTE2: TBlobField;
    qHistoriaClinicaTXDOCENTE3: TBlobField;
    qHistoriaClinicaTXDOCENTE4: TBlobField;
    qHistoriaClinicaTXDOCENTE5: TBlobField;
    SubGrupos: TZTable;
    SubGruposBVISIBLE: TSmallintField;
    SubGruposID: TLongintField;
    SubGruposTRATAMIENTOGRUPO_ID: TLongintField;
    SubGruposTRATAMIENTOSUBGRUPO: TStringField;
    Tratamientos: TRxMemoryData;
    Tratamientosalumno_id1: TStringField;
    Tratamientosano1: TLongintField;
    TratamientosbVisible1: TLongintField;
    Tratamientoscodigo1: TLongintField;
    TratamientosDEL: TZQuery;
    Tratamientosdocente_id1: TStringField;
    TratamientosEvalLegajoSELLXHC: TLongintField;
    TratamientosEvalLegajoSELLXHC_ANO: TLongintField;
    Tratamientosevaluacion1: TFloatField;
    TratamientosEvalLegajoSEL: TZQuery;
    TratamientosEvaluarSELLXALUMNOAPYNOMBRE: TStringField;
    TratamientosEvaluarSELLXALUMNOAPYNOMBRE1: TStringField;
    TratamientosEvaluarSELLXALUMNOLEGAJO: TLongintField;
    TratamientosEvaluarSELLXALUMNOLEGAJO1: TLongintField;
    TratamientosEvaluarSELLXGRUPO: TStringField;
    TratamientosEvaluarSELLXGRUPO1: TStringField;
    TratamientosEvaluarSELLXHC: TLongintField;
    TratamientosEvaluarSELLXHC_ANO: TLongintField;
    TratamientosEvaluarSELLXSUBGRUPO: TStringField;
    TratamientosEvaluarSELLXSUBGRUPO1: TStringField;
    TratamientosEvaluarSELPIEZASDENTALES: TStringField;
    TratamientosEvaluarSELPIEZASDENTALES1: TStringField;
    Tratamientosfecha1: TDateField;
    TratamientosfEvaluacion1: TDateTimeField;
    Tratamientosgrupo_id1: TLongintField;
    Tratamientosid1: TStringField;
    TratamientosINS: TZQuery;
    TratamientoslxAlumnoApyNombre: TStringField;
    TratamientoslxAlumnoLegajo: TLongintField;
    TratamientoslxGrupo: TStringField;
    TratamientoslxHC: TLongintField;
    TratamientoslxHC_ANO: TLongintField;
    TratamientoslxSubGrupo: TStringField;
    Tratamientospaciente_id1: TStringField;
    TratamientospiezasDentales: TStringField;
    TratamientosSEL: TZQuery;
    qHistoriaClinica: TZQuery;
    TratamientosEvaluarSEL: TZQuery;
    qTratamientoPrestacion: TZQuery;
    TratamientosSELALUMNO_ID: TStringField;
    TratamientosSELALUMNO_ID1: TStringField;
    TratamientosSELALUMNO_ID2: TStringField;
    TratamientosSELALUMNO_ID3: TStringField;
    TratamientosSELANO: TLongintField;
    TratamientosSELANO1: TLongintField;
    TratamientosSELANO2: TLongintField;
    TratamientosSELANO3: TLongintField;
    TratamientosSELBVISIBLE: TSmallintField;
    TratamientosSELBVISIBLE1: TSmallintField;
    TratamientosSELBVISIBLE2: TSmallintField;
    TratamientosSELBVISIBLE3: TSmallintField;
    TratamientosSELCODIGO: TLongintField;
    TratamientosSELCODIGO1: TLongintField;
    TratamientosSELCODIGO2: TLongintField;
    TratamientosSELCODIGO3: TLongintField;
    TratamientosSELDOCENTE_ID: TStringField;
    TratamientosSELDOCENTE_ID1: TStringField;
    TratamientosSELDOCENTE_ID2: TStringField;
    TratamientosSELDOCENTE_ID3: TStringField;
    TratamientosSELEVALUACION: TFloatField;
    TratamientosSELEVALUACION1: TFloatField;
    TratamientosSELEVALUACION2: TFloatField;
    TratamientosSELEVALUACION3: TFloatField;
    TratamientosSELFECHA: TDateField;
    TratamientosSELFECHA1: TDateField;
    TratamientosSELFECHA2: TDateField;
    TratamientosSELFECHA3: TDateField;
    TratamientosSELFEVALUACION: TDateField;
    TratamientosSELFEVALUACION1: TDateField;
    TratamientosSELFEVALUACION2: TDateField;
    TratamientosSELFEVALUACION3: TDateField;
    TratamientosSELGRUPO_ID: TLongintField;
    TratamientosSELGRUPO_ID1: TLongintField;
    TratamientosSELGRUPO_ID2: TLongintField;
    TratamientosSELGRUPO_ID3: TLongintField;
    TratamientosSELID: TStringField;
    TratamientosSELID1: TStringField;
    TratamientosSELID2: TStringField;
    TratamientosSELID3: TStringField;
    TratamientosSELPACIENTE_ID: TStringField;
    TratamientosSELPACIENTE_ID1: TStringField;
    TratamientosSELPACIENTE_ID2: TStringField;
    TratamientosSELPACIENTE_ID3: TStringField;
    TratamientosSELPIEZASDENTALES: TStringField;
    TratamientosSELPIEZASDENTALES1: TStringField;
    TratamientosSELSUBGRUPO_ID: TLongintField;
    TratamientosSELSUBGRUPO_ID1: TLongintField;
    TratamientosSELSUBGRUPO_ID2: TLongintField;
    TratamientosSELSUBGRUPO_ID3: TLongintField;
    TratamientosSELTXALUMNO: TBlobField;
    TratamientosSELTXALUMNO1: TBlobField;
    TratamientosSELTXALUMNO2: TBlobField;
    TratamientosSELTXALUMNO3: TBlobField;
    TratamientosSELTXDOCENTE: TBlobField;
    TratamientosSELTXDOCENTE1: TBlobField;
    TratamientosSELTXDOCENTE2: TBlobField;
    TratamientosSELTXDOCENTE3: TBlobField;
    Tratamientossubgrupo_id1: TLongintField;
    TratamientostxAlumno1: TStringField;
    TratamientostxDocente1: TStringField;
    TratamientosUPD: TZQuery;
    Tratamientosalumno_id: TStringField;
    Tratamientosano: TLongintField;
    TratamientosbVisible: TLongintField;
    Tratamientoscodigo: TLongintField;
    Tratamientosdocente_id: TStringField;
    Tratamientosevaluacion: TFloatField;
    Tratamientosfecha: TDateField;
    TratamientosfEvaluacion: TDateTimeField;
    Tratamientosgrupo_id: TLongintField;
    Tratamientosid: TStringField;
    Tratamientospaciente_id: TStringField;
    Tratamientossubgrupo_id: TLongintField;
    TratamientostxAlumno: TStringField;
    TratamientostxDocente: TStringField;
    Grupos: TZTable;
    tugGrupos: TZTable;
    tugSubGrupos: TZTable;
    procedure DataModuleCreate(Sender: TObject);
    procedure TratamientosAfterInsert(DataSet: TDataSet);
  private
    treatID: GUID_ID;
    piezasDentales: string;

    function piezasANros(laspiezas: string): string;
    procedure BuscarHC_query (var consulta: TZQuery; parametro: Variant);
  public
    property dientes: string read piezasDentales;

    procedure Save;
    function New (student_id:GUID_ID): GUID_ID;
    procedure Edit (treatment_id: GUID_ID);
    procedure Del (treatment_id: GUID_ID);

    procedure VincularPaciente (patID: GUID_ID);
    procedure VincularGrupos;
    procedure TreatmentByID (tID: GUID_ID);
    procedure MarcarPiezaDental (idx: integer);

    procedure HistoriaClinica (patID: GUID_ID);
    procedure FiltrarEvaluaciones (fechaIni, fechaFin: TDate; idClinica, idCurso: integer );
    procedure FiltrarEvalLegajo (fechaIni, fechaFin: TDate; legajo: integer );

    procedure BuscarHC_Apellido(apellido: string);
    procedure BuscarHC_Documento(documento: integer);
    procedure BuscarHC_LegAlumno(legajo:integer);
    procedure BuscarHC_ApellidoAlumno(apellido: string);
    procedure BuscarHC_NroHC(nro, year: integer);

    function treatmentByPrest (prestacion: integer): GUID_ID;

  end;

var
  DMTratamientos: TDMTratamientos;

implementation
{$R *.lfm}
uses
  dateutils
  ;

{ TDMTratamientos }

procedure TDMTratamientos.TratamientosAfterInsert(DataSet: TDataSet);
begin
  treatID:= DM_General.CrearGUID;
//  piezasDentales:= EmptyStr;
  piezasDentales:= StringOfChar('0' , MAX_DIENTES);
  Tratamientosid.AsString:= treatID;
  Tratamientoscodigo.asInteger:= -1;
  Tratamientosano.asInteger:= YearOf(Now);
  Tratamientosgrupo_id.AsInteger:= 0;
  Tratamientossubgrupo_id.asInteger:= 0;
  Tratamientosfecha.AsDateTime:= DateOf(Now);
  Tratamientosalumno_id.AsString:= GUIDNULO;
  Tratamientospaciente_id.AsString:= GUIDNULO;
  Tratamientosevaluacion.AsFloat:= -1;
  TratamientosfEvaluacion.AsDateTime:= 0;
  Tratamientosdocente_id.asString:= GUIDNULO;
  TratamientostxAlumno.AsString:= ' ';
  TratamientostxDocente.AsString:= ' ';
  TratamientosbVisible.asInteger:= 1;
  TratamientospiezasDentales.AsString:= piezasDentales;
end;

function TDMTratamientos.piezasANros(laspiezas: string): string;
var
  idx: integer;
  cadena: string;
begin
  cadena:= EmptyStr;
  if Length(laspiezas) = MAX_DIENTES then
  begin
    for idx:= 1 to MAX_DIENTES do
      if laspiezas[idx] = '1' then
       cadena:= cadena + '; ' + IntToStr(idx);
    cadena:= Copy(cadena, 3, Length(cadena)-2);
  end;
  Result:= cadena;
end;


procedure TDMTratamientos.DataModuleCreate(Sender: TObject);
begin
  DMTratamientos.Grupos.Open;
  DMTratamientos.SubGrupos.Open;
end;

procedure TDMTratamientos.Save;
begin
  with Tratamientos do
  begin
    if State in dsEditModes then
     Post;

    if Tratamientos.RecordCount = 1 then
    begin;
      Edit;
      TratamientospiezasDentales.asString:= piezasDentales;
      Post;
    end;

  end;
  DM_General.GrabarDatos(TratamientosSEL, TratamientosINS, TratamientosUPD, Tratamientos, 'id');
end;

function TDMTratamientos.New(student_id:GUID_ID): GUID_ID;
begin
  DM_General.ReiniciarTabla(Tratamientos);
  Tratamientos.Insert;
  Tratamientosalumno_id.asString:= student_id;
end;

procedure TDMTratamientos.Edit(treatment_id: GUID_ID);
begin
  treatID:=treatment_id;
  DM_General.ReiniciarTabla(Tratamientos);
  with TratamientosSEL do
  begin
    if active then close;
    ParamByName('id').asString:= treatment_id;
    Open;
    Tratamientos.LoadFromDataSet(TratamientosSEL, 0, lmAppend);
    piezasDentales:= TratamientosSELPIEZASDENTALES.AsString;
    close;
    Tratamientos.Edit;
  end;
end;

procedure TDMTratamientos.Del(treatment_id: GUID_ID);
begin
  with TratamientosDEL do
  begin
    ParamByName('id').AsString:= treatment_id;
    ExecSQL;
  end;
end;

procedure TDMTratamientos.VincularPaciente(patID: GUID_ID);
begin
  with Tratamientos do
  begin
    Edit;
    Tratamientospaciente_id.asString:= patID;
    Post;
  end;
end;

procedure TDMTratamientos.VincularGrupos;
begin
  with Tratamientos do
  begin
    Edit;
    Tratamientosgrupo_id.asInteger:= GruposID.AsInteger;
    Tratamientossubgrupo_id.AsInteger:= SubGruposID.AsInteger;
    Post;
  end;
end;

procedure TDMTratamientos.TreatmentByID(tID: GUID_ID);
begin
 Edit(tID);
end;

procedure TDMTratamientos.MarcarPiezaDental(idx: integer);
begin
  piezasDentales[idx]:= '1';
end;

procedure TDMTratamientos.HistoriaClinica(patID: GUID_ID);
begin
  DM_General.ReiniciarTabla(HC);
  with qHistoriaClinica do
  begin
    if active then close;
    ParamByName('paciente_id').asString := patID;
    Open;
    HC.LoadFromDataSet(qHistoriaClinica, 0, lmAppend);
    if HC.RecordCount > 0 then
    begin
      HC.First;
      While Not HC.EOF do
      begin
        HC.Edit;
        HCpiezas.AsString:= PiezasANros (HCpiezas.AsString);
        HC.Post;
        HC.Next;
      end;
    end;
  end;
end;

procedure TDMTratamientos.FiltrarEvaluaciones(fechaIni, fechaFin: TDate;
  idClinica, idCurso: integer);
begin
  DM_General.ReiniciarTabla(Tratamientos);
  with TratamientosEvaluarSEL do
  begin
    if active then close;
    ParamByName('fechaIni').asDateTime:= DateOF (fechaIni);
    ParamByName('fechaFin').AsDateTime:= DateOF (fechaFin);
    ParamByName('curso_id').asInteger:= idCurso;
    ParamByName('clinica_id').asInteger:= idClinica;
    Open;
    Tratamientos.LoadFromDataSet(TratamientosEvaluarSEL, 0, lmAppend);
    close;
  end;
end;

procedure TDMTratamientos.FiltrarEvalLegajo(fechaIni, fechaFin: TDate;
  legajo: integer);
begin
  DM_General.ReiniciarTabla(Tratamientos);
  with TratamientosEvalLegajoSEL do
  begin
    if active then close;
    ParamByName('fechaIni').asDateTime:= DateOF (fechaIni);
    ParamByName('fechaFin').AsDateTime:= DateOF (fechaFin);
    ParamByName('legajo').asInteger:= legajo;
    Open;
    Tratamientos.LoadFromDataSet(TratamientosEvalLegajoSEL, 0, lmAppend);
    close;
  end;
end;

procedure TDMTratamientos.BuscarHC_query(var consulta: TZQuery;
  parametro: Variant);
begin
  DM_General.ReiniciarTabla(HC);
  with consulta do
  begin
    if active then close;
    if ParamByName('parametro').DataType = ftInteger then
      ParamByName('parametro').AsInteger:= parametro
    else
      ParamByName('parametro').asString:= parametro;
    Open;
    HC.LoadFromDataSet(consulta, 0, lmAppend);
    close;
  end;
end;


procedure TDMTratamientos.BuscarHC_Apellido(apellido: string);
begin
  BuscarHC_query(qBusHC_ApPaciente, apellido);
end;

procedure TDMTratamientos.BuscarHC_Documento(documento: integer);
begin
  BuscarHC_query(qBusHC_DocPaciente ,documento);
end;

procedure TDMTratamientos.BuscarHC_LegAlumno(legajo: integer);
begin
  BuscarHC_query(qBusHC_LegAlumno, legajo);
end;

procedure TDMTratamientos.BuscarHC_ApellidoAlumno(apellido: string);
begin
  BuscarHC_query(qBusHC_ApAlumno, apellido);
end;

procedure TDMTratamientos.BuscarHC_NroHC(nro, year: integer);
begin
  BuscarHC_query(qBusHC_NroHC, nro);
end;

function TDMTratamientos.treatmentByPrest(prestacion: integer): GUID_ID;
begin
  DM_General.ReiniciarTabla(Tratamientos);
  with qTratamientoPrestacion do
  begin
    if active then close;
    ParamByName('codigo').asInteger:= prestacion;
    ParamByName('ano').asInteger:= YearOf(Now);
    Open;
    if RecordCount > 0 then
    begin
      Tratamientos.LoadFromDataSet(qTratamientoPrestacion, 0, lmAppend);
      Result:=qTratamientoPrestacion.FieldByName('id').asString;
    end
    else
      Result:= GUIDNULO;
    close;
  end;
end;

end.

