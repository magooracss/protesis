unit frm_tratamientoae;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls, DBCtrls, DBGrids, CheckLst
  , dmgeneral, DB, Grids;

type

  { TfrmTratamientoAE }

  TfrmTratamientoAE = class(TForm)
    BitBtn1: TBitBtn;
    btnAgregarTratamiento: TBitBtn;
    btnSave: TBitBtn;
    DBMemo3: TDBMemo;
    Label17: TLabel;
    Label18: TLabel;
    lbPiezasDentales: TCheckListBox;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DS_clinicas: TDatasource;
    DS_Cursos: TDatasource;
    DS_HC: TDatasource;
    DS_Tratamientos: TDatasource;
    DS_Alumnos: TDatasource;
    DS_Pacientes: TDatasource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    edDocumento: TEdit;
    edEdad: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    Splitter1: TSplitter;
    procedure BitBtn1Click(Sender: TObject);
    procedure btnAgregarTratamientoClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure DBGrid1Enter(Sender: TObject);
    procedure DBGrid3PrepareCanvas(sender: TObject; DataCol: Integer;
      Column: TColumn; AState: TGridDrawState);
    procedure DS_PacientesDataChange(Sender: TObject; Field: TField);
    procedure edDocumentoExit(Sender: TObject);
    procedure edDocumentoKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    studentID: GUID_ID;
    treatmentID: GUID_ID;
    treatmentSave: boolean;

    procedure Initialize;

    procedure Buscar(unDocumento: integer);
    procedure NuevoPaciente;
    procedure VincularPiezasDentales;

    function SeguridadValida(nivel: integer): boolean;

  public
    property treatment_id: GUID_ID read treatmentID write treatmentID;
    property student_id: GUID_ID read studentID write studentID;
  end;

var
  frmTratamientoAE: TfrmTratamientoAE;

implementation

{$R *.lfm}
uses
  dm_tratamientos
  , dm_alumnos
  , dm_pacientes
  , frm_pacienteae
  , frm_login
  , dm_docentes
  , SD_Configuracion
  ;

{ TfrmTratamientoAE }


procedure TfrmTratamientoAE.FormShow(Sender: TObject);
begin
  Initialize;
end;

procedure TfrmTratamientoAE.edDocumentoKeyPress(Sender: TObject; var Key: char);
begin
  //  if (NOT (key IN ['0'..'9'])) then
  //   key := '0';
  if key = #13 then
    Buscar(StrToIntDef(edDocumento.Text, 0));
end;

procedure TfrmTratamientoAE.DS_PacientesDataChange(Sender: TObject; Field: TField);
begin
  edEdad.Text := IntToStr(DMPacientes.EdadPaciente);
end;

procedure TfrmTratamientoAE.edDocumentoExit(Sender: TObject);
begin
  if (TRIM(edDocumento.Text) <> EmptyStr) then
    Buscar(StrToIntDef(edDocumento.Text, 0));
end;

procedure TfrmTratamientoAE.btnSaveClick(Sender: TObject);
begin
  if treatmentSave then
  begin
    DMTratamientos.VincularGrupos;
    VincularPiezasDentales;
    DMTratamientos.Save;
  end;

  ModalResult := mrOk;
end;

procedure TfrmTratamientoAE.DBGrid1Enter(Sender: TObject);
begin
  if treatmentSave = false then
  begin
    DMTratamientos.New(studentID);
    DMTratamientos.VincularPaciente(DMPacientes.Pacientesid.AsString);
  end;
  btnAgregarTratamiento.Enabled:= True;
  treatmentSave:= true;
end;

procedure TfrmTratamientoAE.btnAgregarTratamientoClick(Sender: TObject);
var
  patientID: GUID_ID;
  idx: integer;
begin
  patientID:= DMPacientes.Pacientesid.AsString;
  btnAgregarTratamiento.Enabled:= False;
  treatmentSave:= false;

  //Save
  DMTratamientos.VincularGrupos;
  VincularPiezasDentales;
  DMTratamientos.Save;

  DM_General.ReiniciarTabla(DMPacientes.Pacientes);
  DM_General.ReiniciarTabla(DMTratamientos.HC);

  //Cargo las piezas dentales
  lbPiezasDentales.Items.Clear;
  for idx := 0 to MAX_DIENTES - 1 do
    lbPiezasDentales.Items.Add(IntToStr(idx + 1));

  //DMTratamientos.New(studentID);
  DMPacientes.PatientByID(patientID);
  DMTratamientos.VincularPaciente(patientID);

  DMAlumnos.StudentByID(DMTratamientos.Tratamientosalumno_id.AsString);
  DMAlumnos.ClinicName(DMAlumnos.Alumnosclinica_id.AsInteger);
  DMAlumnos.CourseName(DMAlumnos.Alumnoscurso_id.AsInteger);
  DMTratamientos.HistoriaClinica(patientID);
end;

procedure TfrmTratamientoAE.BitBtn1Click(Sender: TObject);
begin
   if SeguridadValida(NIVEL_DOC_TITULAR) then
   begin
     if (MessageDlg ('ATENCION', 'Elimino el tratamiento seleccionado?', mtConfirmation, [mbYes, mbNo],0 ) = mrYes) then
     begin
       DMTratamientos.Del(ds_HC.DataSet.FieldByName('tratamiento_id').AsString);
       DMTratamientos.HistoriaClinica(DMTratamientos.HCpaciente_id.asString);
     end;
   end;
end;

procedure TfrmTratamientoAE.DBGrid3PrepareCanvas(sender: TObject;
  DataCol: Integer; Column: TColumn; AState: TGridDrawState);
begin
    if Column.FieldName='fEvaluacion' then
    begin
      if Column.Field.AsDateTime  = 0 then
      begin
        with (Sender As TDBGrid) do
        begin
          Canvas.Font.Color:=Canvas.Brush.Color;
          Canvas.Font.Style:=[fsBold];
        end;
      end;
    end;

    if Column.FieldName='Evaluacion' then
    begin
      if Column.Field.AsFloat < 4 then
      begin
        with (Sender As TDBGrid) do
        begin
          Canvas.Font.Color:=clRed;
          Canvas.Font.Style:=[fsBold];
        end;
        if Column.Field.AsFloat  = -1 then
        begin
          with (Sender As TDBGrid) do
          begin
            Canvas.Font.Color:=Canvas.Brush.Color;
            Canvas.Font.Style:=[fsBold];
          end;
        end;
      end;
    end;
end;

procedure TfrmTratamientoAE.Initialize;
var
  idx: integer;
  dientes: string;
begin
  DM_General.ReiniciarTabla(DMPacientes.Pacientes);
  DM_General.ReiniciarTabla(DMTratamientos.HC);
  treatmentSave:= false;

  //Cargo las piezas dentales
  lbPiezasDentales.Items.Clear;
  for idx := 0 to MAX_DIENTES - 1 do
    lbPiezasDentales.Items.Add(IntToStr(idx + 1));

  if (treatmentID = GUIDNULO) then
  begin
  //  DMTratamientos.New(studentID)
    DMAlumnos.StudentByID(studentID);
  end
  else
  begin
    DMTratamientos.Edit(treatmentID);
    DMPacientes.PatientByID(DMTratamientos.Tratamientospaciente_id.AsString);
    dientes:= DMTratamientos.TratamientospiezasDentales.asString;
    for idx:= 0 to MAX_DIENTES -1 do
      if dientes[idx + 1] = '1' then
        lbPiezasDentales.Checked[idx]:= true;

    edDocumento.Text:= intToStr(DMPacientes.Pacientesdocumento.AsInteger);
    DMTratamientos.HistoriaClinica(DMTratamientos.Tratamientospaciente_id.AsString);
    DMAlumnos.StudentByID(DMTratamientos.Tratamientosalumno_id.AsString);
  end;


  DMAlumnos.ClinicName(DMAlumnos.Alumnosclinica_id.AsInteger);
  DMAlumnos.CourseName(DMAlumnos.Alumnoscurso_id.AsInteger);
end;

procedure TfrmTratamientoAE.Buscar(unDocumento: integer);
var
  PatID: GUID_ID;
  ok: Boolean;
begin
  PatID := DMPacientes.PatientByDoc(unDocumento);
  if (PatID = GUIDNULO) then
  begin
    if (MessageDlg('ATENCION',
      'El documento ingresado no se encuentra. Lo carga como paciente nuevo?',
      mtConfirmation, [mbNo, mbYes], 0) = mrYes) then
    begin
      ok:= (LeerDato(SECCION_APP, H_P) = LEVANTAR_DATO);
      if (ok) then
      begin
      NuevoPaciente;
      DMTratamientos.VincularPaciente(DMPacientes.Pacientesid.AsString);
      DMPacientes.PatientByID(DMTratamientos.Tratamientospaciente_id.AsString);
      DMTratamientos.Save;
      DMTratamientos.TreatmentByID(DMTratamientos.Tratamientosid.AsString);
      end
      else
      if SeguridadValida(NIVEL_DOC_TITULAR) then
      begin
        NuevoPaciente;
        DMTratamientos.VincularPaciente(DMPacientes.Pacientesid.AsString);
        DMPacientes.PatientByID(DMTratamientos.Tratamientospaciente_id.AsString);
        DMTratamientos.Save;
        DMTratamientos.TreatmentByID(DMTratamientos.Tratamientosid.AsString);
      end
      else
      begin
        ShowMessage('En este período, solamente el titular puede dar de alta pacientes');
      end;
    end
    else
      edDocumento.SetFocus;
  end
  else
  begin
    DMPacientes.PatientByID(PatID);
    if treatmentSave then
    begin
      DMTratamientos.VincularPaciente(PatID);
      DMTratamientos.Save;
    end;
    DMTratamientos.TreatmentByID(DMTratamientos.Tratamientosid.AsString);
  end;
 // DMTratamientos.HistoriaClinica(DMTratamientos.Tratamientospaciente_id.AsString);
 DMTratamientos.HistoriaClinica(DMPacientes.Pacientesid.AsString);
end;

procedure TfrmTratamientoAE.NuevoPaciente;
var
  frm: TfrmPacienteAE;
begin
  frm := TfrmPacienteAE.Create(self);
  try
    frm.patient_id := GUIDNULO;
    frm.patient_doc:= StrToInt(edDocumento.Text);
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TfrmTratamientoAE.VincularPiezasDentales;
var
  idx: integer;
begin
  for idx := 0 to MAX_DIENTES - 1 do
    if lbPiezasDentales.Checked[idx] then
      DMTratamientos.MarcarPiezaDental(StrToInt(lbPiezasDentales.items.Strings[idx]));
end;

function TfrmTratamientoAE.SeguridadValida(nivel: integer): boolean;
var
  pass: TfrmLogin;
begin
  pass := TfrmLogin.Create(self);
  try
    pass.NivelMinimo := nivel;
    Result := (pass.ShowModal = mrOk);
  finally
    pass.Free;
  end;
end;

end.

