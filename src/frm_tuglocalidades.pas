unit frm_tuglocalidades;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, DBGrids, Buttons;

type

  { TfrmTugLocalidades }

  TfrmTugLocalidades = class(TForm)
    btnClose: TBitBtn;
    dsPaises: TDatasource;
    dsProvincias: TDatasource;
    dsLocalidades: TDatasource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    procedure btnCloseClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmTugLocalidades: TfrmTugLocalidades;

implementation
{$R *.lfm}
uses
  dm_pacientes
  ;

{ TfrmTugLocalidades }

procedure TfrmTugLocalidades.btnCloseClick(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

end.

