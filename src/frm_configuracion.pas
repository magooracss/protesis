unit frm_configuracion;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls;

type

  { TfrmConfiguracion }

  TfrmConfiguracion = class(TForm)
    btnGrabar: TBitBtn;
    ckAltaPacientes: TCheckBox;
    procedure btnGrabarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure LevantarConfiguracion;
  public
    { public declarations }
  end;

var
  frmConfiguracion: TfrmConfiguracion;

implementation
{$R *.lfm}
uses
  SD_Configuracion
  ;


{ TfrmConfiguracion }

procedure TfrmConfiguracion.FormShow(Sender: TObject);
begin
  LevantarConfiguracion;
end;

procedure TfrmConfiguracion.btnGrabarClick(Sender: TObject);
begin
  if ckAltaPacientes.Checked then
    EscribirDato(SECCION_APP, H_P, LEVANTAR_DATO)
  else
    EscribirDato(SECCION_APP, H_P, NEGAR_DATO);
  ModalResult:= mrOK;
end;

procedure TfrmConfiguracion.LevantarConfiguracion;
begin
  ckAltaPacientes.Checked:= (LeerDato(SECCION_APP, H_P) = LEVANTAR_DATO);
end;

end.

