unit frm_login;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons
  ,dm_docentes
  ,dmgeneral
  ;

type

  { TfrmLogin }

  TfrmLogin = class(TForm)
    btnOK: TBitBtn;
    edUsuario: TEdit;
    edClave: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnOKClick(Sender: TObject);
    procedure edClaveKeyPress(Sender: TObject; var Key: char);
    procedure edUsuarioKeyPress(Sender: TObject; var Key: char);
  private
    nivelBase: integer;
    nivelUsr: integer;
    TeachID: GUID_ID;
    function Evaluar (usuario, clave: string): boolean;
  public
    property NivelUsuario: integer read nivelUsr;
    property NivelMinimo: integer write nivelBase;

    property TeacherID: GUID_ID read TeachID;
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.lfm}

{ TfrmLogin }

procedure TfrmLogin.btnOKClick(Sender: TObject);
begin
  nivelUsr:= ACCESO_INVALIDO;
  TeachID:= GUIDNULO;
  if Evaluar (trim(edUsuario.Text), trim (edClave.Text)) then
   ModalResult:= mrOK
  else
  begin
    ShowMessage ('Usuario o clave invalida para esta operación');
    edUsuario.SetFocus;
  end;
end;

procedure TfrmLogin.edClaveKeyPress(Sender: TObject; var Key: char);
begin
  if key = #13 then
    btnOKClick(Sender);
end;

procedure TfrmLogin.edUsuarioKeyPress(Sender: TObject; var Key: char);
begin
  if key = #13 then
    edClave.SetFocus;
end;

function TfrmLogin.Evaluar(usuario, clave: string): boolean;
begin
  DMDocentes.DocenteAcceso (usuario, clave, nivelUsr, TeachID);
  Result:= (nivelUsr <> ACCESO_INVALIDO) and (nivelUsr <= nivelBase);
end;

end.

