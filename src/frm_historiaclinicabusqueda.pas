unit frm_historiaclinicabusqueda;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  DBGrids, StdCtrls, Buttons, dmgeneral;

type

  { TfrmHistoriaClinicaBusqueda }

  TfrmHistoriaClinicaBusqueda = class(TForm)
    btnAceptar: TBitBtn;
    btnCancelar: TBitBtn;
    btnBuscar: TBitBtn;
    ds_tratamientos: TDatasource;
    DBGrid1: TDBGrid;
    edDatoBusqueda: TEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    RG_Busqueda: TRadioGroup;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edDatoBusquedaKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    function getPaciente_id: GUID_ID;
    { private declarations }
  public
    property paciente_id: GUID_ID read getPaciente_id;
  end;

var
  frmHistoriaClinicaBusqueda: TfrmHistoriaClinicaBusqueda;

implementation
{$R *.lfm}
uses
   dm_tratamientos
  ,dateutils
  ;

{ TfrmHistoriaClinicaBusqueda }



function TfrmHistoriaClinicaBusqueda.getPaciente_id: GUID_ID;
begin
  Result:= DMTratamientos.HCpaciente_id.AsString;
end;

procedure TfrmHistoriaClinicaBusqueda.btnCancelarClick(Sender: TObject);
begin
  ModalResult:= mrCancel;
end;

procedure TfrmHistoriaClinicaBusqueda.edDatoBusquedaKeyPress(Sender: TObject;
  var Key: char);
begin
  if key = #13 then
    btnBuscarClick(sender);
end;

procedure TfrmHistoriaClinicaBusqueda.FormShow(Sender: TObject);
begin
  DM_General.ReiniciarTabla(DMTratamientos.HC);
end;

procedure TfrmHistoriaClinicaBusqueda.btnBuscarClick(Sender: TObject);
begin
  case RG_Busqueda.ItemIndex of
    0: DMTratamientos.BuscarHC_Documento(StrToIntDef(TRIM(edDatoBusqueda.Text), 0));
    1: DMTratamientos.BuscarHC_Apellido(TRIM(edDatoBusqueda.Text));
    2: DMTratamientos.BuscarHC_LegAlumno(StrToIntDef(TRIM(edDatoBusqueda.Text), 0));
    3: DMTratamientos.BuscarHC_ApellidoAlumno(TRIM(edDatoBusqueda.Text));
    4: DMTratamientos.BuscarHC_NroHC(StrToIntDef(TRIM(edDatoBusqueda.Text), 0), YearOf(Now));
  end;
end;

procedure TfrmHistoriaClinicaBusqueda.btnAceptarClick(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

end.

