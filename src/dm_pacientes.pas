unit dm_pacientes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, rxmemds, ZDataset
  ,dmgeneral
  ;

type

  { TDMPacientes }

  TDMPacientes = class(TDataModule)
    ds_paises: TDatasource;
    ds_provincias: TDatasource;
    PacientesBusDoc: TZQuery;
    PacientesBusDocAPELLIDOS: TStringField;
    PacientesBusDocBVISIBLE: TSmallintField;
    PacientesBusDocDOCUMENTO: TLongintField;
    PacientesBusDocDOCUMENTO_TIPO: TLongintField;
    PacientesBusDocDOMICILIO: TStringField;
    PacientesBusDocEMAIL: TStringField;
    PacientesBusDocFNACIMIENTO: TDateField;
    PacientesBusDocID: TStringField;
    PacientesBusDocNOMBRES: TStringField;
    PacientesBusDocTELEFONO: TStringField;
    Pacienteshc: TLongintField;
    Pacienteshc_ano: TLongintField;
    Pacienteslocalidad_id: TLongintField;
    PacientesSELAPELLIDOS: TStringField;
    PacientesSELBVISIBLE: TSmallintField;
    PacientesSELDOCUMENTO: TLongintField;
    PacientesSELDOCUMENTO_TIPO: TLongintField;
    PacientesSELDOMICILIO: TStringField;
    PacientesSELEMAIL: TStringField;
    PacientesSELFNACIMIENTO: TDateField;
    PacientesSELHC: TLongintField;
    PacientesSELHC_ANO: TLongintField;
    PacientesSELID: TStringField;
    PacientesSELLOCALIDAD_ID: TLongintField;
    PacientesSELNOMBRES: TStringField;
    PacientesSELSEXO: TStringField;
    PacientesSELTELEFONO: TStringField;
    PacientesSexo: TStringField;
    qLocProvPaisPacienteCODIGOPOSTAL: TStringField;
    qLocProvPaisPacienteLOCALIDAD: TStringField;
    qLocProvPaisPacienteLOCALIDAD_ID: TLongintField;
    qLocProvPaisPacientePAIS: TStringField;
    qLocProvPaisPacientePAIS_ID: TLongintField;
    qLocProvPaisPacientePROVINCIA: TStringField;
    qLocProvPaisPacientePROVINCIA_ID: TLongintField;
    qTiposDocumento: TZQuery;
    Pacientes: TRxMemoryData;
    PacientesDEL: TZQuery;
    PacientesINS: TZQuery;
    PacientesSEL: TZQuery;
    PacientesUPD: TZQuery;
    Pacientesapellidos: TStringField;
    PacientesbVisible: TLongintField;
    Pacientesdocumento: TLongintField;
    Pacientesdocumento_tipo: TLongintField;
    Pacientesdomicilio: TStringField;
    Pacientesemail: TStringField;
    PacientesfNacimiento: TDateField;
    Pacientesid: TStringField;
    Pacientesnombres: TStringField;
    Pacientestelefono: TStringField;
    qTiposDocumentoBVISIBLE: TSmallintField;
    qTiposDocumentoDOCUMENTOTIPO: TStringField;
    qTiposDocumentoID: TLongintField;
    qLocProvPaisPaciente: TZQuery;
    qTugProvincias: TZQuery;
    qTugLocalidades: TZQuery;
    tugPaises: TZTable;
    tugProvincias: TZTable;
    tugLocalidades: TZTable;
    qTugPaises: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure PacientesAfterInsert(DataSet: TDataSet);
  private
    patientID: GUID_ID;
  public
    function EdadPaciente: integer;

    procedure Save;
    function New: GUID_ID;
    procedure Edit (patient_id: GUID_ID);
    procedure Del (patient_id: GUID_ID);
    procedure Editcb (localidad, tipodoc: integer);

    function PatientByDoc (document: integer):GUID_ID;
    procedure PatientByID (patID: GUID_ID);

    procedure LocalidadesPacientes (idLocalidad: integer);

  end;

var
  DMPacientes: TDMPacientes;

implementation
{$R *.lfm}
uses
  dateutils
  ;

{ TDMPacientes }

procedure TDMPacientes.PacientesAfterInsert(DataSet: TDataSet);
begin
  patientID:= DM_General.CrearGUID;
  Pacientesid.AsString:= patientID;
  Pacientesapellidos.asString:= EmptyStr;
  Pacientesnombres.asString:= EmptyStr;
  Pacientesdocumento_tipo.asInteger:= 0;
  //Pacientesdocumento.asInteger:= 0;
  PacientesfNacimiento.AsDateTime:= DateOf(Now);
  Pacientesdomicilio.asString:= EmptyStr;
  Pacientestelefono.AsString:= EmptyStr;
  Pacientesemail.AsString:= EmptyStr;
  PacientesbVisible.asInteger:= 1;
  Pacienteshc.asInteger:= -1;
  Pacienteshc_ano.asInteger:= YearOf(Now);
  PacientesSexo.asString:= 'M';
  Pacienteslocalidad_id.asInteger:= 1;
end;

procedure TDMPacientes.DataModuleCreate(Sender: TObject);
begin
  tugPaises.Open;
  tugProvincias.Open;
  tugLocalidades.Open;
end;

function TDMPacientes.EdadPaciente: integer;
begin
  with Pacientes do
  begin
   if Active and (RecordCount > 0) then
   begin
     Result:= YearsBetween(Now, PacientesfNacimiento.AsDateTime);
   end
   else
     Result:= 0;
  end;
end;

procedure TDMPacientes.Save;
begin
  DM_General.GrabarDatos(PacientesSEL, PacientesINS, PacientesUPD, Pacientes, 'id');
end;

function TDMPacientes.New: GUID_ID;
begin
  DM_General.ReiniciarTabla(Pacientes);
  Pacientes.Insert;
  Result:= patientID;
end;

procedure TDMPacientes.Edit(patient_id: GUID_ID);
begin
  patientID:=patient_id;
  DM_General.ReiniciarTabla(Pacientes);
  with PacientesSEL do
  begin
    if active then close;
    ParamByName('id').asString:= patient_id;
    Open;
    Pacientes.LoadFromDataSet(PacientesSEL, 0, lmAppend);
    close;
    Pacientes.Edit;
  end;
end;

procedure TDMPacientes.Del(patient_id: GUID_ID);
begin
  with PacientesDEL do
  begin
    ParamByName('id').asString:= patient_id;
    ExecSQL;
  end;
end;

procedure TDMPacientes.Editcb(localidad, tipodoc: integer);
begin
  With Pacientes do
  begin
    Edit;
    Pacienteslocalidad_id.asInteger:= localidad;
    Pacientesdocumento_tipo.asInteger:= tipodoc;
    Post;
  end;
end;

function TDMPacientes.PatientByDoc(document: integer): GUID_ID;
begin
  with PacientesBusDoc do
  begin
    if active then close;
    ParamByName('documento').asInteger:= document;
    Open;
    if RecordCount > 0 then
      Result:= PacientesBusDocID.AsString
    else
      Result:= GUIDNULO;
  end;
end;

procedure TDMPacientes.PatientByID(patID: GUID_ID);
begin
  DM_General.ReiniciarTabla(Pacientes);
  With PacientesSEL do
  begin
    if active then close;
    ParamByName('id').AsString:= patID;
    Pacientes.LoadFromDataSet(PacientesSEL, 0, lmAppend);
    close;
  end;
end;

procedure TDMPacientes.LocalidadesPacientes(idLocalidad: integer);
begin
  with qLocProvPaisPaciente do
  begin
    if Active then close;
    ParamByName('localidad_id').asInteger:= idLocalidad;
    Open;
  end;
end;

end.

