unit dm_docentes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, rxmemds, ZDataset
  ,dmgeneral
  ;

const
  NIVEL_DOC_TITULAR = 1;
  NIVEL_DOC_CLASE = 5;

  ACCESO_INVALIDO = 99999;

type

  { TDMDocentes }

  TDMDocentes = class(TDataModule)
    DocenteCursoPropioCURSO_ID: TStringField;
    DocenteCursoPropioCURSO_ID1: TStringField;
    DocenteCursoPropioDOCENTE_ID: TStringField;
    DocenteCursoPropioDOCENTE_ID1: TStringField;
    DocenteCursoPropioID: TStringField;
    DocenteCursoPropioID1: TStringField;
    Docentes: TRxMemoryData;
    Docentesapellidos: TStringField;
    DocenteCursoPropio: TZQuery;
    qDocenteAcceso: TZQuery;
    DocentesBusDocAPELLIDOS: TStringField;
    DocentesBusDocCLAVE: TStringField;
    DocentesBusDocDOCUMENTO: TLongintField;
    DocentesBusDocEMAIL: TStringField;
    DocentesBusDocID: TStringField;
    DocentesBusDocNIVEL: TLongintField;
    DocentesBusDocNOMBRES: TStringField;
    DocentesBusDocUSUARIO: TStringField;
    Docentesclave: TStringField;
    DocentesDEL: TZQuery;
    Docentesdocumento: TLongintField;
    Docentesemail: TStringField;
    Docentesid: TStringField;
    DocentesINS: TZQuery;
    Docentesnivel: TLongintField;
    Docentesnombres: TStringField;
    DocentesSEL: TZQuery;
    DocentesUPD: TZQuery;
    Docentesusuario: TStringField;
    DocentesBusDoc: TZQuery;
    qDocenteAccesoAPELLIDOS: TStringField;
    qDocenteAccesoCLAVE: TStringField;
    qDocenteAccesoDOCUMENTO: TLongintField;
    qDocenteAccesoEMAIL: TStringField;
    qDocenteAccesoID: TStringField;
    qDocenteAccesoNIVEL: TLongintField;
    qDocenteAccesoNOMBRES: TStringField;
    qDocenteAccesoUSUARIO: TStringField;
    procedure DocentesAfterInsert(DataSet: TDataSet);
  private
    teacherID: GUID_ID;
  public

    procedure Save;
    function New: GUID_ID;
    procedure Edit (teacher_id: GUID_ID);
    procedure Del (teacher_id: GUID_ID);

    function teacherByDoc (doc: integer): GUID_ID;
    procedure teacherByID (teacher_id: GUID_ID);
    procedure DocenteAcceso(usuario, clave: string; var nivel: integer; var id: GUID_ID);

    function CursoPropio (idCurso: integer):boolean;


  end;

var
  DMDocentes: TDMDocentes;

implementation

{$R *.lfm}

{ TDMDocentes }

procedure TDMDocentes.DocentesAfterInsert(DataSet: TDataSet);
begin
  teacherID:= DM_General.CrearGUID;
  Docentesid.AsString:= teacherID;
  Docentesapellidos.asString:= EmptyStr;
  Docentesnombres.asString:= EmptyStr;
  Docentesdocumento.asInteger:= 0;
  Docentesemail.AsString:= EmptyStr;
  Docentesusuario.asString:= EmptyStr;
  Docentesclave.asString:= EmptyStr;
  Docentesnivel.asInteger:= NIVEL_DOC_CLASE;
end;



procedure TDMDocentes.Save;
begin
  DM_General.GrabarDatos(DocentesSEL, DocentesINS, DocentesUPD, Docentes, 'id');
end;

function TDMDocentes.New: GUID_ID;
begin
  DM_General.ReiniciarTabla(Docentes);
  Docentes.Insert;
  Result:= teacherID;
end;

procedure TDMDocentes.Edit(teacher_id: GUID_ID);
begin
  teacher_id:=teacher_id;
  DM_General.ReiniciarTabla(Docentes);
  with DocentesSEL do
  begin
    if active then close;
    ParamByName('id').asString:= teacher_id;
    Open;
    Docentes.LoadFromDataSet(DocentesSEL, 0, lmAppend);
    close;
    Docentes.Edit;
  end;
end;

procedure TDMDocentes.Del(teacher_id: GUID_ID);
begin
  with DocentesDEL do
  begin
    ParamByName('id').asString:= teacher_id;
    ExecSQL;
  end;
end;

function TDMDocentes.teacherByDoc(doc: integer): GUID_ID;
begin
  with DocentesBusDoc do
  begin
    if active then close;
    ParamByName('documento').asInteger:= doc;
    Open;
    if RecordCount > 0 then
      Result:= DocentesBusDocID.AsString
    else
      Result:= GUIDNULO;
  end;
end;

procedure TDMDocentes.teacherByID(teacher_id: GUID_ID);
begin
  DM_General.ReiniciarTabla(Docentes);
  With DocentesSEL do
  begin
    if active then close;
    ParamByName('id').AsString:= teacher_id;
    Docentes.LoadFromDataSet(DocentesSEL, 0, lmAppend);
    close;
  end;
end;

procedure TDMDocentes.DocenteAcceso(usuario, clave: string; var nivel: integer; var id: GUID_ID);
begin
  with qDocenteAcceso do
  begin
    if active then close;
    ParamByName('usuario').asString:= usuario;
    ParamByName('clave').AsString:= clave;
    Open;
    If RecordCount > 0 then
    begin
       nivel:= qDocenteAccesoNIVEL.asInteger;
       id:= qDocenteAccesoID.asString;
    end
    else
    begin
       nivel:= ACCESO_INVALIDO;
       id:= GUIDNULO;
    end;
    close;
  end;
end;

function TDMDocentes.CursoPropio(idCurso: integer): boolean;
begin
  with DocenteCursoPropio do
  begin
    if active then close;
    ParamByName('docente_id').asString:= Docentesid.AsString;
    ParamByName('curso_id').asInteger:= idCurso;
    Open;
    Result:= (RecordCount > 0);
  end;
end;

end.

