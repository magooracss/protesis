program protesis;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, rxnew, zcomponent, frm_main, dm_conexion, SD_Configuracion, dmgeneral,
  frm_about, frm_alumnoae, dm_alumnos, dm_pacientes, frm_pacienteae,
  frm_tratamientoae, dm_tratamientos, dm_docentes, frm_docenteae,
  frm_evaluaciones, frm_login, frm_historiaclinica, frm_tugTratamientos,
frm_tuglocalidades, frm_historiaclinicabusqueda, frm_configuracion
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='Prótesis';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TDMConexion, DMConexion);
  Application.CreateForm(TDM_General, DM_General);
  Application.CreateForm(TDMAlumnos, DMAlumnos);
  Application.CreateForm(TDMPacientes, DMPacientes);
  Application.CreateForm(TDMTratamientos, DMTratamientos);
  Application.CreateForm(TDMDocentes, DMDocentes);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmHistoriaClinicaBusqueda, frmHistoriaClinicaBusqueda
    );
  Application.CreateForm(TfrmConfiguracion, frmConfiguracion);
  Application.Run;
end.

