unit frm_tugTratamientos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, DBGrids,
  ExtCtrls, StdCtrls, Buttons;

type

  { TfrmTugTratamientos }

  TfrmTugTratamientos = class(TForm)
    btnClose: TBitBtn;
    Datasource1: TDatasource;
    Datasource2: TDatasource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmTugTratamientos: TfrmTugTratamientos;

implementation
{$R *.lfm}
uses
  dm_tratamientos

  ;

{ TfrmTugTratamientos }

procedure TfrmTugTratamientos.FormCreate(Sender: TObject);
begin
  DMTratamientos.tugGrupos.Open;
  DMTratamientos.tugSubGrupos.Open;
end;

procedure TfrmTugTratamientos.btnCloseClick(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

end.

