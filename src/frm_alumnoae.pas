unit frm_alumnoae;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  DbCtrls, StdCtrls, Buttons
  ,dmgeneral, db;

type

  { TfrmAlumnosAE }

  TfrmAlumnosAE = class(TForm)
    btnSave: TBitBtn;
    cbCourse: TComboBox;
    cbClinic: TComboBox;
    dsAlumnos: TDatasource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    procedure btnSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    studentID: GUID_ID;
    procedure Initialize;
  public
    property student_id: GUID_ID read studentID write studentID;
  end;

var
  frmAlumnosAE: TfrmAlumnosAE;

implementation
{$R *.lfm}
uses
  dm_alumnos
 ;

{ TfrmAlumnosAE }

procedure TfrmAlumnosAE.btnSaveClick(Sender: TObject);
begin
  DMAlumnos.EditCb(DM_General.obtenerIDIntComboBox(cbCourse)
                  ,DM_General.obtenerIDIntComboBox(cbClinic)
                  );
  DMAlumnos.Save;
  ModalResult:=mrOK;
end;

procedure TfrmAlumnosAE.FormShow(Sender: TObject);
begin
  Initialize;
end;

procedure TfrmAlumnosAE.Initialize;
begin
  DM_General.CargarComboBox(cbCourse, 'curso','id', DMAlumnos.qCourses);
  DM_General.CargarComboBox(cbClinic, 'clinica', 'id', DMAlumnos.qClinics);
  if studentID = GUIDNULO then
    studentID:= DMAlumnos.New
  else
  begin
    DMAlumnos.Edit (studentID);
    cbCourse.ItemIndex:= DM_General.obtenerIdxCombo(cbCourse, DMAlumnos.Alumnoscurso_id.asInteger);
    cbClinic.ItemIndex:= DM_General.obtenerIdxCombo(cbClinic, DMAlumnos.Alumnosclinica_id.asInteger);
  end;
end;

end.

