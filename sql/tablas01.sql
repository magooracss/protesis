CREATE DOMAIN "guid"
  AS VARCHAR(38)
  CHARACTER SET ASCII
  DEFAULT '{00000000-0000-0000-0000-000000000000}' NOT NULL;


CREATE TABLE Alumnos
( 
  id		"guid" NOT NULL
, apellidos     varchar(500)
, nombres	varchar(500)
, legajo	int	default 0
, barra		int 	default 0
, documento	int	default 0
, curso_id	int	default 0
, clinica_id	int	default 0
);


CREATE TABLE Cursos
(
  id		     int	default -1
, curso		     varchar (300) 
, materia_id	     int     	default 0
, bVisible           smallint 	default 1
);

INSERT INTO Cursos
(id, curso, materia_id, bVisible)
VALUES
(0,'Desconocido', 0, 1);

INSERT INTO Cursos
(id, curso, materia_id, bVisible)
VALUES
(1, 'Curso I', 1, 1);

INSERT INTO Cursos
(id, curso, materia_id, bVisible)
VALUES
(2, 'Curso II', 1, 1);

INSERT INTO Cursos
(id, curso, materia_id,  bVisible)
VALUES
(3, 'Curso III', 1, 1);

CREATE GENERATOR GenIdCursos;
SET GENERATOR GenIdCursos TO 3;

SET TERM ^ ;

CREATE TRIGGER idCursos FOR Cursos
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenidCursos,1);
END^


SET TERM ; ^

CREATE TABLE Docentes
(
  id		"guid" NOT NULL
, apellidos	varchar(500)
, nombres	varchar(500)
, email		varchar(200)
, usuario	varchar(20)
, clave		varchar(20)
, documento	integer default 0
, nivel		int	default 5
);


CREATE TABLE CursoDocentes
(
  id		"guid" NOT NULL
, curso_id	"guid"
, docente_id	"guid"
);


CREATE TABLE Clinicas
(
  id		     int	default -1
, clinica	     varchar (100) 
, bVisible           smallint 	default 1
);

INSERT INTO Clinicas
(id, clinica, bVisible)
VALUES
(0,'Desconocida', 1);

INSERT INTO Clinicas
(id, clinica, bVisible)
VALUES
(1,'Clinica 1', 1);

CREATE GENERATOR GenIdClinica;
SET GENERATOR GenIdClinica TO 1;

SET TERM ^ ;

CREATE TRIGGER idClinicas FOR Clinicas
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenidClinica,1);
END^

SET TERM ; ^


CREATE TABLE Materias
(
  id		     int	default -1
, materia	     varchar (300) 
, titular	     varchar (600)
, bVisible           smallint 	default 1
);

INSERT INTO Materias
(id, materia, titular, bVisible)
VALUES
(0,'Desconocida', 'Desconocido',1);

INSERT INTO Materias
(id, materia, titular, bVisible)
VALUES
(1,'Protesis','Gabriel Lazo', 1);

CREATE GENERATOR GenIdMateria;
SET GENERATOR GenIdMateria TO 1;

SET TERM ^ ;

CREATE TRIGGER idMaterias FOR Materias
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenidMateria,1);
END^

SET TERM ; ^

CREATE TABLE Pacientes
(
  id		"guid" NOT NULL
, apellidos	varchar(500)
, nombres	varchar(500)
, documento_tipo int	default 0
, documento	int	default 0
, fNacimiento	date
, domicilio	varchar(2000)
, telefono	varchar(500)
, email		varchar(200)
, hc    	int    default -1
, hc_ano	int    default 0
, bVisible	smallint default 1
, sexo varchar(1) default 'M'
, localidad_id integer default 2
);

CREATE GENERATOR GenHistoriaClinica;
SET GENERATOR GenHistoriaClinica TO 0;

SET TERM ^ ;

CREATE TRIGGER HistoriaClinica FOR Pacientes
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.hc = -1) then
   New.hc = GEN_ID(GenHistoriaClinica,1);
END^

SET TERM ; ^


CREATE TABLE DocumentoTipos
(
  id		     int	default -1
, documentoTipo	     varchar (50) 
, bVisible           smallint 	default 1
);

INSERT INTO DocumentoTipos
(id, documentoTipo, bVisible)
VALUES
(0,'Desconocido',1);

INSERT INTO DocumentoTipos
(id, documentoTipo, bVisible)
VALUES
(1,'DNI', 1);

INSERT INTO DocumentoTipos
(id, documentoTipo, bVisible)
VALUES
(2,'Pasaporte', 1);

CREATE GENERATOR GenIdDocumentoTipo;
SET GENERATOR GenIdDocumentoTipo TO 2;

SET TERM ^ ;

CREATE TRIGGER idDocumentoTipo FOR DocumentoTipos
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenidDocumentoTipo,1);
END^

SET TERM ; ^

CREATE TABLE Tratamientos
(
  id		"guid" NOT NULL
, codigo	int    default -1
, ano		int    default 0
, grupo_id	int    default 0
, subgrupo_id	int    default 0
, fecha		date
, alumno_id	"guid"
, paciente_id	"guid"
, evaluacion	float	default -1
, fEvaluacion	date
, docente_id	"guid"
, txAlumno	blob
, txDocente	blob
, bVisible	smallint default 1
, piezasdentales varchar(100)
);

CREATE GENERATOR GenTratamientoNro;
SET GENERATOR GenTratamientoNro TO 0;

SET TERM ^ ;

CREATE TRIGGER TratamientoNro FOR Tratamientos
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.codigo = -1) then
   New.codigo = GEN_ID(GenTratamientoNro,1);
END^

SET TERM ; ^

CREATE TABLE TratamientoGrupos
(
  id		     int	default -1
, tratamientoGrupo   varchar (150) 
, bVisible           smallint 	default 1
);

INSERT INTO TratamientoGrupos
(id, tratamientoGrupo, bVisible)
VALUES
(0,'Desconocido',1);

CREATE GENERATOR GenIdTratamientoGrupo;
SET GENERATOR GenIdTratamientoGrupo TO 1;

SET TERM ^ ;

CREATE TRIGGER idTratamientoGrupo FOR TratamientoGrupos
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenIdTratamientoGrupo,1);
END^

SET TERM ; ^

CREATE TABLE TratamientoSubGrupos
(
  id		     int	default -1
, tratamientoSubGrupo   varchar (150) 
, tratamientoGrupo_id	int	default 0
, bVisible           smallint 	default 1
);

INSERT INTO TratamientoSubGrupos
(id, tratamientoSubGrupo, tratamientoGrupo_id, bVisible)
VALUES
(0,'Desconocido', 0, 1);

CREATE GENERATOR GenIdTratamientoSubGrupo;
SET GENERATOR GenIdTratamientoSubGrupo TO 1;

SET TERM ^ ;

CREATE TRIGGER idTratamientoSubGrupo FOR TratamientoSubGrupos
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenIdTratamientoSubGrupo,1);
END^

SET TERM ; ^

CREATE TABLE Paises
(
  id		     int	default -1
, Pais           varchar (150) 
, bVisible       smallint 	default 1
);

CREATE TABLE Provincias
(
  id		     int	default -1
, Provincia      varchar (150)
, pais_id        int default 0 
, bVisible       smallint 	default 1
);

CREATE TABLE Localidades
(
  id		     int	default -1
, Localidad      varchar (300)
, codigoPostal   varchar(20)
, provincia_id   int default 0
, bVisible       smallint 	default 1
);


CREATE GENERATOR GenIdPaises;
CREATE GENERATOR GenIdProvincias;
CREATE GENERATOR GenIdLocalidades;


SET GENERATOR GenIdPaises TO 1;
SET GENERATOR GenIdProvincias TO 1;
SET GENERATOR GenIdLocalidades TO 1;

SET TERM ^ ;

CREATE TRIGGER idPaises FOR Paises
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenIdPaises,1);
END^

CREATE TRIGGER idProvincias FOR Provincias
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenIdProvincias,1);
END^

CREATE TRIGGER idLocalidades FOR Localidades
BEFORE INSERT POSITION 0
AS 
BEGIN 
    If (New.id = -1) then
   New.id = GEN_ID(GenIdLocalidades,1);
END^

SET TERM ; ^

